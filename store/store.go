package store

import (
	"errors"

	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/store/sqlstore"
)

// Force to write all unimplemented methods
var _ Store = (*sqlstore.SQLStore)(nil)

// New Store
func New(dbType string, dbSource string) (Store, *model.AppError) {
	var store Store

	switch dbType {
	case "mysql":
		store = &sqlstore.SQLStore{}
	default:
		return nil, model.NewInternalAppError("store.store", "New", errors.New("database type not found"))
	}

	if err := store.Init(dbType, dbSource); err != nil {
		return nil, err
	}
	return store, nil
}

// Store interface
type Store interface {
	Init(dbType string, dbSource string) *model.AppError
	Close() *model.AppError
	Migrate() *model.AppError
	IsMigrated() bool

	GetUsers() (*model.Users, *model.AppError)
	GetUser(userID uint64) (*model.User, *model.AppError)
	GetUserByName(name string) (*model.User, *model.AppError)
	GetUserByEmail(email string) (*model.User, *model.AppError)
	SaveUser(o *model.User) (uint64, *model.AppError)
	UpdateUser(o *model.User) *model.AppError
	DeleteUser(userID uint64) *model.AppError

	GetSoftwares() (*model.Softwares, *model.AppError)
	GetSoftwaresByCreator(userID uint64) (*model.Softwares, *model.AppError)
	GetSoftware(softwareID uint64) (*model.Software, *model.AppError)
	GetSoftwareByName(name string) (*model.Software, *model.AppError)
	GetSoftwareByURL(url string) (*model.Software, *model.AppError)
	SaveSoftware(o *model.Software) (uint64, *model.AppError)
	UpdateSoftware(o *model.Software) *model.AppError
	DeleteSoftware(softwareID uint64) *model.AppError
	SearchSoftwares(query string) (*model.Softwares, *model.AppError)

	GetCategories() (*model.Categories, *model.AppError)
	SaveCategory(o *model.Category) *model.AppError
	DeleteCategory(categoryName string) *model.AppError

	GetSoftwareCategories(softwareID uint64) (*model.Categories, *model.AppError)
	GetCategorySoftwares(categoryName string) (*model.Softwares, *model.AppError)
	SaveSoftwareCategory(softwareID uint64, categoryName string) *model.AppError
	DeleteSoftwareCategory(softwareID uint64, categoryName string) *model.AppError

	GetSubscriptions(userID uint64) (*model.Subscriptions, *model.AppError)
	GetSubscription(userID uint64, softwareID uint64) (*model.Subscription, *model.AppError)
	SaveSubscription(userID uint64, softwareID uint64, o *model.Subscription) *model.AppError
	UpdateSubscription(userID uint64, softwareID uint64, o *model.Subscription) *model.AppError
	DeleteSubscription(userID uint64, softwareID uint64) *model.AppError
	GetUsersWithSubscriptionTo(softwareID uint64) (*model.Users, *model.AppError)

	SaveSubscriptionTag(userID uint64, softwareID uint64, tagID uint64) *model.AppError
	DeleteSubscriptionTag(userID uint64, softwareID uint64, tagID uint64) *model.AppError

	GetUserTags(userID uint64) (*model.Tags, *model.AppError)
	GetUserTag(userID uint64, tagID uint64) (*model.Tag, *model.AppError)
	SaveUserTag(userID uint64, o *model.Tag) (uint64, *model.AppError)
	UpdateUserTag(userID uint64, o *model.Tag) *model.AppError
	DeleteUserTag(userID uint64, tagID uint64) *model.AppError
}
