package sqlstore

import (
	"strings"

	"gitlab.com/vtrack/vtrack-server/model"
)

// GetCategories query
func (s *SQLStore) GetCategories() (*model.Categories, *model.AppError) {
	o := &model.Categories{}
	if err := s.DB.Select(o, "SELECT * FROM category"); err != nil {
		return o, NewStoreError("store.sqlstore.category", "GetCategories", err)
	}
	return o, nil
}

// SaveCategory query
func (s *SQLStore) SaveCategory(o *model.Category) *model.AppError {
	if err := o.PreSave(); err != nil {
		return err
	}
	if _, err := s.DB.Exec("INSERT INTO category (name) VALUES (?)", strings.ToLower(o.Name)); err != nil {
		return NewStoreError("store.sqlstore.category", "SaveCategory", err)
	}
	return nil
}

// DeleteCategory query
func (s *SQLStore) DeleteCategory(categoryName string) *model.AppError {
	if _, err := s.DB.Exec("DELETE FROM category WHERE name = ?", strings.ToLower(categoryName)); err != nil {
		return NewStoreError("store.sqlstore.category", "DeleteCategory", err)
	}
	return nil
}
