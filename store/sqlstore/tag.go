package sqlstore

import "gitlab.com/vtrack/vtrack-server/model"

// GetUserTags query
func (s *SQLStore) GetUserTags(userID uint64) (*model.Tags, *model.AppError) {
	o := &model.Tags{}
	if err := s.DB.Select(o, "SELECT * FROM tag WHERE appuser_id = ?", userID); err != nil {
		return nil, NewStoreError("store.sqlstore.tag", "GetUserTags", err)
	}
	return o, nil
}

// GetUserTag query
func (s *SQLStore) GetUserTag(userID uint64, tagID uint64) (*model.Tag, *model.AppError) {
	o := &model.Tag{}
	if err := s.DB.Get(o, "SELECT * FROM tag WHERE appuser_id = ? AND id = ?", userID, tagID); err != nil {
		return nil, NewStoreError("store.sqlstore.tag", "GetUserTag", err)
	}
	return o, nil
}

// SaveUserTag query
func (s *SQLStore) SaveUserTag(userID uint64, o *model.Tag) (uint64, *model.AppError) {
	result, err := s.DB.Exec("INSERT INTO tag (name, appuser_id, color, text_light) VALUES (?, ?, ?, ?)", o.Name, userID, o.Color, o.TextLight)
	if err != nil {
		return 0, NewStoreError("store.sqlstore.tag", "SaveUserTag", err)
	}
	return extractLastInsertID(result), nil
}

// UpdateUserTag query
func (s *SQLStore) UpdateUserTag(userID uint64, o *model.Tag) *model.AppError {
	if _, err := s.DB.Exec("UPDATE tag SET name = ?, color = ?, text_light = ? WHERE appuser_id = ? AND name = ?", o.Name, o.Color, o.TextLight, userID, o.Name); err != nil {
		return NewStoreError("store.sqlstore.tag", "UpdateUserTag", err)
	}
	return nil
}

// DeleteUserTag query
func (s *SQLStore) DeleteUserTag(userID uint64, tagID uint64) *model.AppError {
	if _, err := s.DB.Exec("DELETE FROM tag WHERE appuser_id = ? AND id = ?", userID, tagID); err != nil {
		return NewStoreError("store.sqlstore.tag", "DeleteUserTag", err)
	}
	return nil
}
