package sqlstore

import "gitlab.com/vtrack/vtrack-server/model"

// GetSoftwares query
func (s *SQLStore) GetSoftwares() (*model.Softwares, *model.AppError) {
	o := &model.Softwares{}
	if err := s.DB.Select(o, "SELECT * FROM software"); err != nil {
		return nil, NewStoreError("store.sqlstore.software", "GetSoftwares", err)
	}
	return o, nil
}

// GetSoftwaresByCreator query
func (s *SQLStore) GetSoftwaresByCreator(userID uint64) (*model.Softwares, *model.AppError) {
	o := &model.Softwares{}
	if err := s.DB.Select(o, "SELECT * FROM software WHERE created_by = ?", userID); err != nil {
		return nil, NewStoreError("store.sqlstore.software", "GetSoftwaresByCreator", err)
	}
	return o, nil
}

// GetSoftware query
func (s *SQLStore) GetSoftware(softwareID uint64) (*model.Software, *model.AppError) {
	o := &model.Software{}
	if err := s.DB.Get(o, "SELECT * FROM software WHERE id = ? LIMIT 1", softwareID); err != nil {
		return nil, NewStoreError("store.sqlstore.software", "GetSoftware", err)
	}
	return o, nil
}

// GetSoftwareByName query
func (s *SQLStore) GetSoftwareByName(name string) (*model.Software, *model.AppError) {
	o := &model.Software{}
	if err := s.DB.Get(o, "SELECT * FROM software WHERE name = ? LIMIT 1", name); err != nil {
		return nil, NewStoreError("store.sqlstore.software", "GetSoftwareByName", err)
	}
	return o, nil
}

// GetSoftwareByURL query
func (s *SQLStore) GetSoftwareByURL(url string) (*model.Software, *model.AppError) {
	o := &model.Software{}
	if err := s.DB.Get(o, "SELECT * FROM software WHERE repository = ? LIMIT 1", url); err != nil {
		return nil, NewStoreError("store.sqlstore.software", "GetSoftwareByURL", err)
	}
	return o, nil
}

// SaveSoftware query
func (s *SQLStore) SaveSoftware(o *model.Software) (uint64, *model.AppError) {
	result, err := s.DB.Exec("INSERT INTO software(name, description, repository, repository_type, website, version, version_updated_at, created_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", o.Name, o.Description, o.Repository, o.RepositoryType, o.Website, o.Version, o.VersionUpdatedAt, o.CreatedBy)
	if err != nil {
		return 0, NewStoreError("store.sqlstore.software", "SaveSoftware", err)
	}
	return extractLastInsertID(result), nil
}

// UpdateSoftware query
func (s *SQLStore) UpdateSoftware(o *model.Software) *model.AppError {
	if _, err := s.DB.Exec("UPDATE software SET name = ?, description = ?, repository = ?, repository_type = ?, website = ?, version = ?, version_updated_at = ?, created_by = ? WHERE id = ?", o.Name, o.Description, o.Repository, o.RepositoryType, o.Website, o.Version, o.VersionUpdatedAt, o.CreatedBy, o.ID); err != nil {
		return NewStoreError("store.sqlstore.software", "UpdateSoftware", err)
	}
	return nil
}

// DeleteSoftware query
func (s *SQLStore) DeleteSoftware(softwareID uint64) *model.AppError {
	if _, err := s.DB.Exec("DELETE FROM software WHERE id = ?", softwareID); err != nil {
		return NewStoreError("store.sqlstore.software", "DeleteSoftware", err)
	}
	return nil
}

// SearchSoftwares query
func (s *SQLStore) SearchSoftwares(query string) (*model.Softwares, *model.AppError) {
	o := &model.Softwares{}

	sqlString := `SELECT software.*
FROM software
WHERE software.name LIKE ?`

	if err := s.DB.Select(o, sqlString, "%"+query+"%"); err != nil {
		return nil, NewStoreError("store.sqlstore.software", "SearchSoftwares", err)
	}

	return o, nil
}
