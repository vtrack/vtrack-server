package sqlstore

import "gitlab.com/vtrack/vtrack-server/model"

// GetSoftwareCategories query
func (s *SQLStore) GetSoftwareCategories(softwareID uint64) (*model.Categories, *model.AppError) {
	o := &model.Categories{}

	sql := `SELECT category.*
	FROM category, software_category
	WHERE category.name = software_category.category_name
	AND software_category.software_id = ?`

	if err := s.DB.Select(o, sql, softwareID); err != nil {
		return nil, NewStoreError("store.sqlstore.software", "GetSoftwareCategories", err)
	}
	return o, nil
}

// GetCategorySoftwares query
func (s *SQLStore) GetCategorySoftwares(categoryName string) (*model.Softwares, *model.AppError) {
	o := &model.Softwares{}

	sql := `SELECT software.*
		FROM software, software_category
		WHERE software.id = software_category.software_id
		AND software_category.category_name = ?`

	if err := s.DB.Select(o, sql, categoryName); err != nil {
		return nil, NewStoreError("store.sqlstore.software", "GetCategorySoftwares", err)
	}
	return o, nil
}

// SaveSoftwareCategory query
func (s *SQLStore) SaveSoftwareCategory(softwareID uint64, categoryName string) *model.AppError {
	if _, err := s.DB.Exec("INSERT INTO software_category(software_id, category_name) VALUES (?, ?)", softwareID, categoryName); err != nil {
		return NewStoreError("store.sqlstore.software_category", "SaveSoftwareCategory", err)
	}
	return nil
}

// DeleteSoftwareCategory query
func (s *SQLStore) DeleteSoftwareCategory(softwareID uint64, categoryName string) *model.AppError {
	if _, err := s.DB.Exec("DELETE FROM software_category WHERE software_id = ? AND category_name = ?", softwareID, categoryName); err != nil {
		return NewStoreError("store.sqlstore.software_category", "DeleteSoftwareCategory", err)
	}
	return nil
}
