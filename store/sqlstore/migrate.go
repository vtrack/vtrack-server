package sqlstore

import (
	"gitlab.com/vtrack/vtrack-server/model"
)

var dropSchema = []string{
	"DROP TABLE IF EXISTS subscription_tag",
	"DROP TABLE IF EXISTS subscription",
	"DROP TABLE IF EXISTS tag",
	"DROP TABLE IF EXISTS software_category",
	"DROP TABLE IF EXISTS software",
	"DROP TABLE IF EXISTS category",
	"DROP TABLE IF EXISTS appuser",
}

var createSchema = []string{
	`CREATE TABLE appuser (
			id INT NOT NULL AUTO_INCREMENT,
			username VARCHAR(64) NOT NULL,
			email VARCHAR(128) NOT NULL,
			email_verified BOOLEAN NOT NULL,
			password_hash VARCHAR(255) NOT NULL,
			roles VARCHAR(255) NOT NULL,

			created_at TIMESTAMP NOT NULL DEFAULT NOW(),
			updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),

			CONSTRAINT appuser_pk PRIMARY KEY (id),
			CONSTRAINT appuser_uq UNIQUE (username, email)
		)`,
	`CREATE TABLE software (
			id INT NOT NULL AUTO_INCREMENT,
			name VARCHAR(64) NOT NULL UNIQUE,
			repository VARCHAR(255) NOT NULL,
			repository_type VARCHAR(255) NOT NULL,
			website VARCHAR(255),
			description VARCHAR(255) NOT NULL,
			version VARCHAR(255),
			version_updated_at TIMESTAMP,

			created_by INT NOT NULL,

			created_at TIMESTAMP NOT NULL DEFAULT NOW(),
			updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),

			CONSTRAINT software_fk_appuser FOREIGN KEY (created_by) REFERENCES appuser(id),
			CONSTRAINT software_pk PRIMARY KEY (id),
			CONSTRAINT software_uq UNIQUE (repository)
		)`,
	`CREATE TABLE category (
			name VARCHAR(64) NOT NULL,
			PRIMARY KEY (name)
		)`,
	`CREATE TABLE software_category (
			software_id INT NOT NULL,
			category_name VARCHAR(64) NOT NULL,

			CONSTRAINT softwarecategory_fk_software FOREIGN KEY (software_id) REFERENCES software(id),
			CONSTRAINT softwarecategory_fk_category FOREIGN KEY (category_name) REFERENCES category(name),
			CONSTRAINT softwarecategory_pk PRIMARY KEY (software_id,category_name)
		)`,
	`CREATE TABLE subscription (
			appuser_id INT NOT NULL,
			software_id INT NOT NULL,
			severity VARCHAR(5) NOT NULL,

			created_at TIMESTAMP NOT NULL DEFAULT NOW(),
			updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),

			CONSTRAINT subscription_fk_appuser FOREIGN KEY (appuser_id) REFERENCES appuser(id) ON DELETE CASCADE,
			CONSTRAINT subscription_fk_software FOREIGN KEY (software_id) REFERENCES software(id),
			CONSTRAINT subscription_pk PRIMARY KEY (appuser_id,software_id)
		)`,
	`CREATE TABLE tag (
			id INT NOT NULL AUTO_INCREMENT,
			appuser_id INT NOT NULL,
			name VARCHAR(255) NOT NULL UNIQUE,
			color VARCHAR(255) NOT NULL,
			text_light BOOLEAN,

			created_at TIMESTAMP NOT NULL DEFAULT NOW(),
			updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),

			CONSTRAINT tag_fk_appuser FOREIGN KEY (appuser_id) REFERENCES appuser(id) ON DELETE CASCADE,
			CONSTRAINT tag_pk PRIMARY KEY (id, appuser_id)
		)`,
	`CREATE TABLE subscription_tag (
			software_id INT NOT NULL,
			tag_id INT NOT NULL,
			appuser_id INT NOT NULL,

			CONSTRAINT subscriptiontag_fk_software FOREIGN KEY (software_id) REFERENCES software(id),
			CONSTRAINT subscriptiontag_fk_tag FOREIGN KEY (tag_id, appuser_id) REFERENCES tag(id, appuser_id) ON DELETE CASCADE,
			CONSTRAINT subscriptiontag_pk PRIMARY KEY (software_id, tag_id, appuser_id)
		)`,
}

// Migrate store
func (s *SQLStore) Migrate() *model.AppError {
	for _, sql := range dropSchema {
		s.DB.MustExec(sql)
	}
	for _, sql := range createSchema {
		s.DB.MustExec(sql)
	}
	return nil
}

// IsMigrated check wether the database is empty
func (s *SQLStore) IsMigrated() bool {
	if _, err := s.DB.Exec("SELECT 1 FROM appuser LIMIT 1"); err != nil {
		return false
	}
	return true
}
