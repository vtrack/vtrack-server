package sqlstore

import (
	"gitlab.com/vtrack/vtrack-server/model"
)

// GetUsers query
func (s *SQLStore) GetUsers() (*model.Users, *model.AppError) {
	o := &model.Users{}
	if err := s.DB.Select(o, "SELECT * FROM appuser"); err != nil {
		return nil, NewStoreError("store.sqlstore.user", "GetUsers", err)
	}
	return o, nil
}

// GetUser query
func (s *SQLStore) GetUser(userID uint64) (*model.User, *model.AppError) {
	o := &model.User{}
	if err := s.DB.Get(o, "SELECT * FROM appuser WHERE id = ? LIMIT 1", userID); err != nil {
		return nil, NewStoreError("store.sqlstore.user", "GetUser", err)
	}
	return o, nil
}

// GetUserByName query
func (s *SQLStore) GetUserByName(username string) (*model.User, *model.AppError) {
	o := &model.User{}
	if err := s.DB.Get(o, "SELECT * FROM appuser WHERE username = ? LIMIT 1", username); err != nil {
		return nil, NewStoreError("store.sqlstore.user", "GetUserByName", err)
	}
	return o, nil
}

// GetUserByEmail query
func (s *SQLStore) GetUserByEmail(email string) (*model.User, *model.AppError) {
	o := &model.User{}
	if err := s.DB.Get(o, "SELECT * FROM appuser WHERE email = ? LIMIT 1", email); err != nil {
		return nil, NewStoreError("store.sqlstore.user", "GetUserByEmail", err)
	}
	return o, nil
}

// SaveUser query
func (s *SQLStore) SaveUser(o *model.User) (uint64, *model.AppError) {
	if err := o.PreSave(); err != nil {
		return 0, err
	}
	result, err := s.DB.Exec("INSERT INTO appuser(username, email,email_verified, password_hash, roles) VALUES (?, ?, ?, ?, ?)", o.Username, o.Email, o.EmailVerified, o.PasswordHash, o.Roles)
	if err != nil {
		return 0, NewStoreError("store.sqlstore.user", "SaveUser", err)
	}
	return extractLastInsertID(result), nil

}

// UpdateUser query
func (s *SQLStore) UpdateUser(o *model.User) *model.AppError {
	if _, err := s.DB.Exec("UPDATE appuser SET username = ?, email = ?, email_verified = ?, password_hash = ?, roles = ? WHERE id = ?", o.Username, o.Email, o.EmailVerified, o.PasswordHash, o.Roles, o.ID); err != nil { // TODO
		return NewStoreError("store.sqlstore.user", "UpdateUser", err)
	}
	return nil
}

// DeleteUser query
func (s *SQLStore) DeleteUser(userID uint64) *model.AppError {

	softwares, err := s.GetSoftwaresByCreator(userID)
	if err != nil {
		return err
	}

	for _, software := range *softwares {
		software.CreatedBy = 1
		if err := s.UpdateSoftware(&software); err != nil {
			return err
		}
	}

	if _, err := s.DB.Exec("DELETE FROM appuser WHERE id = ?", userID); err != nil { // TODO
		return NewStoreError("store.sqlstore.user", "DeleteUser", err)
	}
	return nil
}
