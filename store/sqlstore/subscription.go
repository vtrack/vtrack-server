package sqlstore

import (
	"gitlab.com/vtrack/vtrack-server/model"
)

// GetSubscriptions query
func (s *SQLStore) GetSubscriptions(userID uint64) (*model.Subscriptions, *model.AppError) {
	o := &model.Subscriptions{}
	if err := s.DB.Select(o, "SELECT * FROM subscription WHERE appuser_id = ?", userID); err != nil {
		return nil, NewStoreError("store.sqlstore.subscription", "GetSubscriptions", err)
	}

	for i := range *o {
		software, err := s.GetSoftware((*o)[i].SoftwareID)
		if err != nil {
			return nil, err
		}
		(*o)[i].Software = software

		tags, err := s.GetSubscriptionTags(userID, (*o)[i].SoftwareID)
		if err != nil {
			return nil, err
		}
		(*o)[i].Tags = tags
	}
	return o, nil
}

// GetSubscription query
func (s *SQLStore) GetSubscription(userID, softwareID uint64) (*model.Subscription, *model.AppError) {
	o := &model.Subscription{}
	if err := s.DB.Get(o, "SELECT * FROM subscription WHERE appuser_id = ? AND software_id = ?", userID, softwareID); err != nil {
		return nil, NewStoreError("store.sqlstore.subscription", "GetSubscription", err)
	}

	software, err := s.GetSoftware(o.SoftwareID)
	if err != nil {
		return nil, err
	}
	o.Software = software

	return o, nil
}

// SaveSubscription query
func (s *SQLStore) SaveSubscription(userID, softwareID uint64, o *model.Subscription) *model.AppError {
	if _, err := s.DB.Exec("INSERT INTO subscription(appuser_id, software_id, severity) VALUES (?, ?, ?)", userID, softwareID, o.Severity); err != nil {
		return NewStoreError("store.sqlstore.subscription", "SaveSubscription", err)
	}
	return nil
}

// UpdateSubscription query
func (s *SQLStore) UpdateSubscription(userID, softwareID uint64, o *model.Subscription) *model.AppError {
	if _, err := s.DB.Exec("UPDATE subscription SET severity = ? WHERE appuser_id = ? AND software_id = ?", o.Severity, userID, softwareID); err != nil {
		return NewStoreError("store.sqlstore.subscription", "UpdateSubscription", err)
	}
	return nil
}

// DeleteSubscription query
func (s *SQLStore) DeleteSubscription(userID, softwareID uint64) *model.AppError {
	if _, err := s.DB.Exec("DELETE FROM subscription WHERE appuser_id = ? AND software_id = ?", userID, softwareID); err != nil {
		return NewStoreError("store.sqlstore.subscription", "DeleteSubscription", err)
	}
	return nil
}

// GetUsersWithSubscriptionTo query
func (s *SQLStore) GetUsersWithSubscriptionTo(softwareID uint64) (*model.Users, *model.AppError) {
	o := &model.Users{}

	sql := `SELECT appuser.*
		FROM appuser, subscription
		WHERE appuser.id = subscription.appuser_id
		AND subscription.software_id = ?`

	if err := s.DB.Select(o, sql, softwareID); err != nil {
		return nil, model.NewInternalAppError("store.sqlstore.subscription", "GetUsersWithSubscriptionTo", err)
	}
	return o, nil
}

// GetSubscriptionTags query
func (s *SQLStore) GetSubscriptionTags(userID, softwareID uint64) (*model.Tags, *model.AppError) {
	o := &model.Tags{}

	sql := `SELECT tag.*
		FROM tag, subscription_tag
		WHERE subscription_tag.appuser_id = ?
		AND subscription_tag.software_id = ?
		AND subscription_tag.tag_id = id`

	if err := s.DB.Select(o, sql, userID, softwareID); err != nil {
		return nil, NewStoreError("store.sqlstore.subscription", "GetSubscriptionTags", err)
	}
	return o, nil
}

// SaveSubscriptionTag query
func (s *SQLStore) SaveSubscriptionTag(userID, softwareID, tagID uint64) *model.AppError {
	if _, err := s.DB.Exec("INSERT INTO subscription_tag(appuser_id, software_id, tag_id) VALUES (?, ?, ?)", userID, softwareID, tagID); err != nil {
		return NewStoreError("store.sqlstore.subscription", "SaveSubscriptionTag", err)
	}
	return nil
}

// DeleteSubscriptionTag query
func (s *SQLStore) DeleteSubscriptionTag(userID, softwareID, tagID uint64) *model.AppError {
	if _, err := s.DB.Exec("DELETE FROM subscription_tag WHERE appuser_id = ? AND software_id = ?, tag_id = ?", userID, softwareID, tagID); err != nil {
		return NewStoreError("store.sqlstore.subscription", "DeleteSubscriptionTag", err)
	}
	return nil
}
