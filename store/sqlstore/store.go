package sqlstore

import (
	"database/sql"
	"time"

	// Import mysql driver
	_ "github.com/go-sql-driver/mysql"

	"github.com/jmoiron/sqlx"
	"gitlab.com/vtrack/vtrack-server/model"
)

// SQLStore struct
type SQLStore struct {
	DB *sqlx.DB
}

// Init store
func (s *SQLStore) Init(dbType string, dbSource string) *model.AppError {
	var err error

	for i := 0; i < 5; i++ {
		s.DB, err = sqlx.Connect(dbType, dbSource)
		if err == nil {
			break
		}
		time.Sleep(4 * time.Second)
	}
	if err != nil {
		return NewStoreError("store.sqlstore.store", "Init", err)
	}

	return nil
}

// Close store
func (s *SQLStore) Close() *model.AppError {
	if err := s.DB.Close(); err != nil {
		return NewStoreError("store.sqlstore.store", "Close", err)
	}
	return nil
}

func extractLastInsertID(r sql.Result) uint64 {
	id, _ := r.LastInsertId()
	return uint64(id)
}
