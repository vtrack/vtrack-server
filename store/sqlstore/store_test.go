package sqlstore

import (
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"github.com/ory/dockertest"
	"gitlab.com/vtrack/vtrack-server/model"
)

var db = &SQLStore{}

func TestMain(m *testing.M) {
	if os.Getenv("CI") == "" { // Local testing
		pool, err := dockertest.NewPool("")
		pool.MaxWait = time.Minute * 2
		if err != nil {
			log.Fatalf("Could not connect to docker: %s", err)
		}

		log.Println("Pulling and running mariadb:10.3 container")
		resource, err := pool.Run("mariadb", "10.3", []string{"MYSQL_ROOT_PASSWORD=root"})
		if err != nil {
			log.Fatalf("Could not start resource: %s", err)
		}

		log.Println("Waiting container to be ready")
		time.Sleep(5 * time.Second)

		log.Println("Connecting to DB")
		if err := pool.Retry(func() error {
			if err := db.Init("mysql", fmt.Sprintf("root:root@tcp(127.0.0.1:%s)/mysql?parseTime=true", resource.GetPort("3306/tcp"))); err != nil {
				return err.InternalError
			}
			return nil
		}); err != nil {
			log.Fatalf("Could not connect to docker: %s", err)
		}

		if err := db.Migrate(); err != nil {
			log.Fatalf("Could not migrate database: %s", err)
		}

		code := m.Run()

		if err := pool.Purge(resource); err != nil {
			log.Fatalf("Could not purge resource: %s", err)
		}

		os.Exit(code)
	} else { // Gitlab CI testing

		if err := db.Init("mysql", "root:root@tcp(mysql:3306)/mysql?parseTime=true"); err != nil {
			log.Fatalf("Could not connect to database: %s", err)
		}

		if err := db.Migrate(); err != nil {
			log.Fatalf("Could not migrate database: %s", err)
		}

		os.Exit(m.Run())
	}
}

//////////////////// User testing ////////////////////

var testUsers = model.Users{
	{ID: 1, Username: "admin", Email: "admin@email.com", Password: "hashedpassword", Roles: "admin"},
	{ID: 2, Username: "username", Email: "username@email.com", Password: "hashedpassword"},
	{ID: 3, Username: "username2", Email: "username2@email.com", Password: "hashedpassword"},
}

var testCategories = model.Categories{
	{Name: "audio"},
	{Name: "chat"},
	{Name: "web"},
}

var testSoftwares = model.Softwares{
	{ID: 1, Name: "Mattermost", Description: "Mattermost description", Repository: "https://github.com/mattermost/mattermost-server", RepositoryType: "github", Website: "https://mattermost.com/", CreatedBy: 1},
	{ID: 2, Name: "Airsonic", Description: "Airsonic description", Repository: "https://github.com/airsonic/airsonic", RepositoryType: "github", Website: "https://airsonic.github.io/", CreatedBy: 2},
}

var testUserTags = model.Tags{
	{ID: 1, Name: "aparis", Color: "red"},
	{ID: 2, Name: "bwebservers", Color: "blue"},
	{ID: 3, Name: "cweb clients", Color: "green"},
}

func compareUser(i1 *model.User, i2 *model.User) bool {
	return i1.ID != i2.ID ||
		i1.Username != i2.Username ||
		i1.Email != i2.Email ||
		i1.Roles != i2.Roles
}

func TestSaveUser(t *testing.T) {
	for _, data := range testUsers {
		if _, err := db.SaveUser(&data); err != nil {
			t.Error(err)
		}
	}
}

func TestGetUser(t *testing.T) {
	for _, data := range testUsers {
		if result, err := db.GetUser(data.ID); err != nil {
			t.Error(err)
		} else {
			if compareUser(result, &data) {
				t.Error(result)
				t.Error(data)
			}
		}
	}
}

func TestGetUserByName(t *testing.T) {
	for _, data := range testUsers {
		if result, err := db.GetUserByName(data.Username); err != nil {
			t.Error(err)
		} else {
			if compareUser(result, &data) {
				t.Error(data)
			}
		}
	}
}

func TestGetUserByEmail(t *testing.T) {
	for _, data := range testUsers {
		if result, err := db.GetUserByEmail(data.Email); err != nil {
			t.Error(err)
		} else {
			if compareUser(result, &data) {
				t.Error(data)
			}
		}
	}
}

func TestGetUserAll(t *testing.T) {
	if result, err := db.GetUsers(); err != nil {
		t.Error(err)
	} else {
		for i, data := range *result {
			if compareUser(&data, &testUsers[i]) {
				t.Error(data)
			}
		}
	}
}

func TestUpdateUser(t *testing.T) {
	id := 1
	testUsers[id].Email = "newemail@email.com"
	testUsers[id].Username = "newusername"
	o := testUsers[id]
	if err := db.UpdateUser(&o); err != nil {
		t.Error(err)
	} else {
		if result, err := db.GetUser(o.ID); err != nil {
			t.Error(err)
		} else {
			if compareUser(result, &testUsers[id]) {
				t.Error(testUsers[id])
			}
		}
	}
}

func TestDeleteUser(t *testing.T) {
	var id uint64 = 3
	if err := db.DeleteUser(id); err != nil {
		t.Error(err)
	} else {
		if _, err := db.GetUser(id); err != nil {
			if err.InternalError.Error() != "sql: no rows in result set" {
				t.Error(err.String())
			}
		}
	}
}

//////////////////// Category testing ////////////////////

func compareCategory(i1 *model.Category, i2 *model.Category) bool {
	return i1.Name != i2.Name
}

func TestSaveCategory(t *testing.T) {
	for _, data := range testCategories {
		if err := db.SaveCategory(&data); err != nil {
			t.Error(err)
		}
	}
}

func TestGetCategories(t *testing.T) {
	if result, err := db.GetCategories(); err != nil {
		t.Error(err)
	} else {
		if len(*result) != 3 {
			t.Errorf("expected result length: 3, found: %d", len(*result))
		}
		for i, data := range *result {
			if compareCategory(&data, &testCategories[i]) {
				t.Error(data)
			}
		}
	}
}

func TestDeleteCategory(t *testing.T) {
	if err := db.DeleteCategory(testCategories[1].Name); err != nil {
		t.Error(err)
	} else {
		if result, err := db.GetCategories(); err != nil {
			t.Error(err)
		} else {
			if len(*result) != 2 {
				t.Errorf("expected result length: 2, found: %d", len(*result))
			}
		}
	}
}

//////////////////// Software testing ////////////////////

func compareSoftware(i1 *model.Software, i2 *model.Software) bool {
	return i1.ID != i2.ID ||
		i1.Name != i2.Name ||
		i1.Description != i2.Description ||
		i1.Repository != i2.Repository ||
		i1.Website != i2.Website ||
		i1.CreatedBy != i2.CreatedBy
}

func TestSaveSoftware(t *testing.T) {
	for _, data := range testSoftwares {
		if err := data.IsValid(); err != nil {
			t.Error(err)
		}
		if _, err := db.SaveSoftware(&data); err != nil {
			t.Error(err)
		}
	}
}

func TestGetSoftware(t *testing.T) {
	for _, data := range testSoftwares {
		if result, err := db.GetSoftware(data.ID); err != nil {
			t.Error(err)
		} else {
			if compareSoftware(result, &data) {
				t.Error(data)
			}
		}
	}
}

func TestGetSoftwareByName(t *testing.T) {
	for _, data := range testSoftwares {
		if result, err := db.GetSoftwareByName(data.Name); err != nil {
			t.Error(err)
		} else {
			if compareSoftware(result, &data) {
				t.Error(data)
			}
		}
	}
}

func TestGetSoftwareByURL(t *testing.T) {
	for _, data := range testSoftwares {
		if result, err := db.GetSoftwareByURL(data.Repository); err != nil {
			t.Error(err)
		} else {
			if compareSoftware(result, &data) {
				t.Error(data)
			}
		}
	}
}

func TestGetSoftwares(t *testing.T) {
	if result, err := db.GetSoftwares(); err != nil {
		t.Error(err)
	} else {
		if len(*result) != 2 {
			t.Errorf("expected result length: 2, found: %d", len(*result))
		}
		for i, data := range *result {
			if compareSoftware(&data, &testSoftwares[i]) {
				t.Error(data)
			}
		}
	}
}

func TestGetSoftwaresByCreator(t *testing.T) {
	var userID uint64 = 1
	if r, err := db.GetSoftwaresByCreator(userID); err != nil {
		t.Error(err)
	} else {
		result := *r // Couldn't get pointer working with a slice
		if len(result) != 1 {
			t.Errorf("expected result length: 1, found: %d", len(result))
		}
		if compareSoftware(&result[0], &testSoftwares[0]) {
			t.Error(&result[0])
		}
	}
}

func TestUpdateSoftware(t *testing.T) {
	id := 1
	testSoftwares[id].Description = "New description"
	testSoftwares[id].Name = "New name"
	o := testSoftwares[id]
	if err := db.UpdateSoftware(&o); err != nil {
		t.Error(err)
	} else {
		if result, err := db.GetSoftware(o.ID); err != nil {
			t.Error(err)
		} else {
			if compareSoftware(result, &testSoftwares[id]) {
				t.Error(testSoftwares[id])
			}
		}
	}
}

func TestDeleteSoftware(t *testing.T) {
	var id uint64 = 1
	if err := db.DeleteSoftware(id); err != nil {
		t.Error(err)
	} else {
		if _, err := db.GetSoftware(id); err != nil {
			if err.InternalError.Error() != "sql: no rows in result set" {
				t.Error(err.String())
			}
		}
	}
}

//////////////////// Software Category testing ////////////////////

func TestSaveSoftwareCategory(t *testing.T) {
	if err := db.SaveSoftwareCategory(2, testCategories[0].Name); err != nil {
		t.Error(err)
	}
	if err := db.SaveSoftwareCategory(2, testCategories[2].Name); err != nil {
		t.Error(err)
	}
}

func TestDeleteSoftwareCategory(t *testing.T) {
	if err := db.DeleteSoftwareCategory(2, testCategories[2].Name); err != nil {
		t.Error(err)
	}
}

func TestGetCategorySoftwares(t *testing.T) {
	if result, err := db.GetCategorySoftwares(testCategories[0].Name); err != nil {
		t.Error(err)
	} else {
		if len(*result) != 1 {
			t.Errorf("expected result length: 1, found: %d", len(*result))
		}
	}
}

func TestGetSoftwareCategories(t *testing.T) {
	if result, err := db.GetSoftwareCategories(2); err != nil {
		t.Error(err)
	} else {
		if len(*result) != 1 {
			t.Errorf("expected result length: 1, found: %d", len(*result))
		}
	}
}

//////////////////// User Tags testing ////////////////////

func TestSaveUserTag(t *testing.T) {
	if _, err := db.SaveUserTag(2, &testUserTags[0]); err != nil {
		t.Error(err)
	}
	if _, err := db.SaveUserTag(2, &testUserTags[1]); err != nil {
		t.Error(err)
	}
	if _, err := db.SaveUserTag(1, &testUserTags[2]); err != nil {
		t.Error(err)
	}
}

func TestUpdateUserTag(t *testing.T) {
	id := 1
	testUserTags[id].Color = "New Color"
	o := testUserTags[id]
	if err := db.UpdateUserTag(2, &testUserTags[id]); err != nil {
		t.Error(err)
	} else {
		if result, err := db.GetUserTags(2); err != nil {
			t.Error(err)
		} else {
			if len(*result) != 2 {
				t.Errorf("expected result length: 2, found: %d", len(*result))
			} else {
				r := *result
				if o.Color != r[id].Color {
					t.Errorf("expected result color: %s, found: %s", o.Color, r[0].Color)
				}
			}
		}
	}
}

func TestDeleteUserTag(t *testing.T) {
	if err := db.DeleteUserTag(2, testUserTags[0].ID); err != nil {
		t.Error(err)
	}
}

func TestGetUserTags(t *testing.T) {
	if result, err := db.GetUserTags(2); err != nil {
		t.Error(err)
	} else {
		if len(*result) != 1 {
			t.Errorf("expected result length: 1, found: %d", len(*result))
		}
	}
}
