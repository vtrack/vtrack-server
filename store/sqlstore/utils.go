package sqlstore

import (
	"database/sql"
	"net/http"
	"strings"

	"gitlab.com/vtrack/vtrack-server/model"
)

// NewStoreError define which status code to return given the SQL error that has been throwd
func NewStoreError(path string, in string, err error) *model.AppError {
	switch {
	case strings.HasPrefix(err.Error(), "Error 1062"): // Duplicate error
		return model.NewAppError(path, in, err, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

	case strings.HasPrefix(err.Error(), "Error 1452"): // Constraint error
		return model.NewAppError(path, in, err, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

	case err == sql.ErrNoRows: // Empty error
		return model.NewAppError(path, in, err, http.StatusText(http.StatusNotFound), http.StatusNotFound)
	}

	return model.NewInternalAppError(path, in, err)
}
