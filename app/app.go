package app

import (
	"gitlab.com/vtrack/vtrack-server/clog"
	"gitlab.com/vtrack/vtrack-server/conf"
	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/store"
)

// App structure
type App struct {
	Config      *conf.Config
	Logger      *clog.Logger
	Store       store.Store
	Runner      *Runner
	EmailClient *EmailClient
	Crawlers    map[string]Crawler

	IsRunning struct {
		Crawler bool
	}

	ErrorCh chan *model.AppError
}

// Init app
func Init() *App {
	// 1. Logger
	// 2. ErrorCh
	// 3. Configuration
	// 4. Email client
	// 5. Database
	// 6. Job runner

	var err *model.AppError

	app := &App{}
	app.Logger, err = clog.New()
	if err != nil {
		panic(err.String())
	}

	app.ErrorCh = make(chan *model.AppError)
	go func(errorCh <-chan *model.AppError) {
		for err := range errorCh {
			app.Logger.Error("app", err)
		}
	}(app.ErrorCh)

	app.Logger.Info("app", "Starting the app...")

	app.Logger.Info("app", "Loading configuration")
	if app.Config, err = conf.Load(); err != nil {
		app.Logger.Fatal("app", err.String())
	}

	app.Logger.Info("app", "Loading email client")
	if app.EmailClient, err = NewEmailClient(
		app.Config.SMTP.Host,
		app.Config.SMTP.Port,
		app.Config.SMTP.Username,
		app.Config.SMTP.Password,
		app.Config.SMTP.From,
		app.ErrorCh,
	); err != nil {
		app.Logger.Error("app", err.String())
	}

	app.Logger.Info("app", "Loading database")
	app.Store, err = store.New(app.Config.Database.Type, app.Config.Database.Source)
	if err != nil {
		app.Logger.Fatal("app", err.String())
	}

	if !app.Store.IsMigrated() {
		app.Logger.Info("app", "Migrating database")
		if err = app.Store.Migrate(); err != nil {
			app.Logger.Fatal("app", err.String())
		}

		app.Logger.Info("app", "Loading default data")
		if err = app.LoadDefaultData(); err != nil {
			app.Logger.Fatal("app", err.String())
		}
	}

	app.Logger.Info("app", "Loading crawlers")
	app.LoadCrawlers()

	app.Logger.Info("app", "Start crawling softwares job")
	if err := app.CrawlSoftwares(); err != nil {
		app.ErrorCh <- err
	}

	app.Logger.Info("app", "Loading job runner")
	if app.Runner, err = NewRunner(app.Config.Location, app.ErrorCh); err != nil {
		app.Logger.Fatal("app", err.String())
	}

	app.Runner.Cron.AddFunc("@hourly", func() {
		app.Logger.Info("app", "Start crawling softwares job")
		if err := app.CrawlSoftwares(); err != nil {
			app.ErrorCh <- err
		}
	})

	app.Runner.Cron.AddFunc("@daily", func() {
		app.Logger.Info("app", "Start houskeeping job")
		if err := app.Housekeeping(); err != nil {
			app.ErrorCh <- err
		}
	})

	return app
}

// Close the app
func (app *App) Close() {
	// 6. Job runner
	// 5. Database
	// 4. Email client
	// 3. Configuration
	// 2. ErrorCh
	// 1. Logger

	app.Logger.Info("app", "Stopping app...")

	app.Logger.Info("app", "Closing job runner")
	app.Runner.Close()

	app.Logger.Info("app", "Closing database")
	if err := app.Store.Close(); err != nil {
		app.Logger.Error("app", err)
	}

	app.Logger.Info("app", "Closing email sender")
	app.EmailClient.Close()

	close(app.ErrorCh)

	app.Logger.Info("app", "Closing logger")
	if err := app.Logger.Close(); err != nil {
		panic(err)
	}
}
