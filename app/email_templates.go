package app

import (
	"bytes"

	"github.com/alecthomas/template"
	"gitlab.com/vtrack/vtrack-server/model"
)

const (
	// TemplateEmailNotificationsFile define the path to the EmailNotification template file
	TemplateEmailNotificationsFile = "templates/email_notifications.gohtml"
)

var (
	templates *template.Template = template.Must(template.Must(template.ParseGlob("templates/layouts/*")).ParseGlob("templates/includes/*"))
)

// Confirmation structure
type Confirmation struct {
	model.User
	Link string
}

// NewNotificationsEmail email
func NewNotificationsEmail(data *model.UserNotifications) (string, string, *model.AppError) {
	body, err := fromTemplate(TemplateEmailNotificationsFile, data)
	return "vTrack - New software(s) version(s)", body, err
}

func fromTemplate(templateFile string, data interface{}) (string, *model.AppError) {
	cloned, err := templates.Clone()
	if err != nil {
		return "", model.NewInternalAppError("app.email_templates", "fromTemplate", err)
	}

	// From global templates parse content template and Execute it with data
	t, err := cloned.ParseFiles(templateFile)
	if err != nil {
		return "", model.NewInternalAppError("app.email_templates", "fromTemplate", err)
	}

	buf := bytes.NewBufferString("")
	err = t.ExecuteTemplate(buf, "base", data)
	if err != nil {
		return "", model.NewInternalAppError("app.email_templates", "fromTemplate", err)
	}

	return buf.String(), nil
}
