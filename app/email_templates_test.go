package app

import (
	"fmt"
	"io/ioutil"
	"testing"

	"github.com/sergi/go-diff/diffmatchpatch"
	"gitlab.com/vtrack/vtrack-server/model"
	_ "gitlab.com/vtrack/vtrack-server/test"
)

func TestNewNotificationsEmail(t *testing.T) {
	data := &model.UserNotifications{
		User: &model.User{
			Username: "username",
			Email:    "username@email.com",
		},
		Notifications: []*model.Notification{
			{
				Software: model.Software{
					ID:          1,
					Name:        "Mattermost",
					Description: "Mattermost description",
					Repository:  "https://github.com/mattermost/mattermost-server",
					Website:     "https://mattermost.com/",
					Version:     "5.6.2",
				},
				NewVersion: model.Version{Major: "5", Minor: "6", Patch: "3"},
			},
			{
				Software: model.Software{
					ID:          2,
					Name:        "Airsonic",
					Description: "Airsonic description",
					Repository:  "https://github.com/airsonic/airsonic",
					Website:     "https://airsonic.github.io/",
					Version:     "10.1.2",
				},
				NewVersion: model.Version{Major: "10", Minor: "2", Patch: "0"},
			},
		},
	}

	_, result, err := NewNotificationsEmail(data)
	if err != nil {
		t.Error(err)
	}

	if expectedBytes, err := ioutil.ReadFile("templates/email_notifications_test.gohtml"); err != nil {
		t.Error(err)
	} else {
		if result != string(expectedBytes) {
			dmp := diffmatchpatch.New()
			diffs := dmp.DiffMain(result, string(expectedBytes), false)
			fmt.Println(diffs)
			// fmt.Println(result)
			t.Fail()
		}
	}
}
