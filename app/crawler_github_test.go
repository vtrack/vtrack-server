package app

import (
	"net/url"
	"os"
	"testing"

	"github.com/subosito/gotenv"
)

func TestCrawlerGithub(t *testing.T) {

	gotenv.Load()

	c := NewCrawlerGithub(os.Getenv("GITHUB_CLIENT_ID"), os.Getenv("GITHUB_CLIENT_SECRET"))

	parsedURL, err := url.Parse("https://github.com/jooola/test")
	if err != nil {
		t.Error(err)
	}

	if v, err := c.FetchVersion(parsedURL); err != nil {
		if err.InternalError.Error() != "reached github api limit" {
			t.Error(err.String())
		}
	} else {
		if v.String() != "1.1.4" {
			t.Errorf("expected: %s, found: %s\n", "1.1.4", v.String())
		}
	}
}
