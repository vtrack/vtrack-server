package app

import (
	"time"

	"gitlab.com/vtrack/vtrack-server/model"
	gomail "gopkg.in/gomail.v2"
)

// EmailClient struct
type EmailClient struct {
	emailCh chan *gomail.Message
	dialer  *gomail.Dialer

	host     string
	port     int
	username string
	password string
	from     string
}

// NewEmailClient creates a new client and returns it into the app
func NewEmailClient(host string, port int, username string, password string, from string, errorCh chan<- *model.AppError) (*EmailClient, *model.AppError) {
	c := &EmailClient{
		host:     host,
		port:     port,
		username: username,
		password: password,
		from:     from,
	}

	c.emailCh = make(chan *gomail.Message)
	c.dialer = gomail.NewDialer(c.host, c.port, c.username, c.password)

	go func(errorCh chan<- *model.AppError) {
		var sendCloser gomail.SendCloser
		var err error
		open := false

		for {
			select {
			case m, ok := <-c.emailCh:
				if !ok {
					return
				}

				if !open {
					if sendCloser, err = c.dialer.Dial(); err != nil {
						errorCh <- model.NewInternalAppError("app.c_email", "Init", err)
						return
					}
					open = true
				}
				if err := gomail.Send(sendCloser, m); err != nil {
					errorCh <- model.NewInternalAppError("app.c_email", "Init", err)
					return
				}
			case <-time.After(30 * time.Second):
				if open {
					if err := sendCloser.Close(); err != nil {
						errorCh <- model.NewInternalAppError("app.c_email", "Init", err)
						return
					}
					open = false
				}
			}
		}
	}(errorCh)

	return c, nil
}

// Send new notifications trough email client
func (s *EmailClient) Send(to string, subject string, body string) *model.AppError {
	m := gomail.NewMessage()
	m.SetHeader("From", s.from)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)
	s.emailCh <- m
	return nil
}

// Close email client
func (s *EmailClient) Close() {
	close(s.emailCh)
}
