package app

import (
	"errors"
	"testing"

	"gitlab.com/vtrack/vtrack-server/model"
)

func TestRunner(t *testing.T) {
	errorCh := make(chan *model.AppError)

	r, err := NewRunner("", errorCh)
	if err != nil {
		t.Error(err)
	}

	r.Cron.AddFunc("* * * * *", func() {
		errorCh <- model.NewInternalAppError("path", "in", errors.New("test"))
		close(errorCh)
	})

	chError := <-errorCh
	if chError.InternalError.Error() != "test" {
		t.Error(chError)
	}
}
