package app

import (
	"io/ioutil"

	"gitlab.com/vtrack/vtrack-server/model"
	"gopkg.in/yaml.v2"
)

// DefaultData struct
type DefaultData struct {
	Softwares []struct {
		Name           string   `yaml:"name"`
		Description    string   `yaml:"description"`
		Website        string   `yaml:"website"`
		Repository     string   `yaml:"repository"`
		RepositoryType string   `yaml:"repository_type"`
		Categories     []string `yaml:"categories"`
	} `yaml:"softwares"`
}

// LoadDefaultData save the first system user and load the predifined softwares and categories
func (a *App) LoadDefaultData() *model.AppError {
	// Creating system user ID 1
	if _, err := a.Store.SaveUser(&model.User{
		Username:      "system",
		Email:         "",
		EmailVerified: true,
		Password:      "system",
		Roles:         model.RoleAdmin,
	}); err != nil {
		return err
	}

	defaultDataBytes, err := ioutil.ReadFile("default_data.yml")
	if err != nil {
		return model.NewInternalAppError("app.default_data", "LoadDefaultData", err)
	}

	defaultData := &DefaultData{}
	if err := yaml.Unmarshal([]byte(defaultDataBytes), defaultData); err != nil {
		return model.NewInternalAppError("app.default_data", "LoadDefaultData", err)
	}

	categories := make(map[string]struct{})
	for _, software := range defaultData.Softwares {
		for _, category := range software.Categories {
			categories[category] = struct{}{}
		}
	}

	for category := range categories {
		a.Store.SaveCategory(&model.Category{Name: category})
	}

	for _, software := range defaultData.Softwares {
		s := &model.Software{
			Name:           software.Name,
			Description:    software.Description,
			Website:        software.Website,
			Repository:     software.Repository,
			RepositoryType: software.RepositoryType,
			CreatedBy:      1, // System user
		}

		if err := s.IsValid(); err != nil {
			return err
		}

		softwareID, err := a.Store.SaveSoftware(s)
		if err != nil {
			return err
		}
		for _, category := range software.Categories {
			a.Store.SaveSoftwareCategory(softwareID, category)
		}
	}

	return nil
}
