package app

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"
	"time"

	"gitlab.com/vtrack/vtrack-server/model"
)

// Get all softwares
// For each softwares
// 		If new version
//				Push to new notification
// For each notifications
// 		Get all users from notification software
//		For each users
//				Push notification to notificationMap
// For each notificationMap
// 		Send email with grouped notifications to user

// Crawler define all the required function for a version crawler
type Crawler interface {
	FetchVersion(sourceURL *url.URL) (*model.Version, *model.AppError)
}

// LoadCrawlers creates the crawlers and add push them into the app
func (a *App) LoadCrawlers() {
	a.Crawlers = make(map[string]Crawler)
	a.Crawlers[model.RepositoryTypeGithub] = NewCrawlerGithub(a.Config.GithubClientID, a.Config.GithubClientSecret)
}

// CrawlSoftwares get all softwares and get latest version to compare
func (a *App) CrawlSoftwares() *model.AppError {
	a.IsRunning.Crawler = true
	defer func() {
		a.IsRunning.Crawler = false
	}()

	softwares, err := a.Store.GetSoftwares()
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	notificationCh := a.DispatchNotifications()

	for _, software := range *softwares {
		wg.Add(1)
		go func(notificationCh chan *model.Notification, software model.Software) {
			defer wg.Done()
			a.CrawlSoftware(notificationCh, software)
		}(notificationCh, software)
	}

	wg.Wait()
	close(notificationCh)

	return nil
}

// CrawlSoftware check latest software version and push a new notification in a channel
func (a *App) CrawlSoftware(notificationCh chan<- *model.Notification, software model.Software) {
	parsedSoftwareURL, err := url.Parse(software.Repository)
	if err != nil {
		a.ErrorCh <- model.NewInternalAppError("app.crawler", "CrawlSoftware", err)
	}

	var version *model.Version
	var appErr *model.AppError
	switch software.RepositoryType {
	case model.RepositoryTypeGithub:
		version, appErr = a.Crawlers[model.RepositoryTypeGithub].FetchVersion(parsedSoftwareURL)
	default:
		a.ErrorCh <- model.NewInternalAppError("app.crawler", "CrawlSoftware", errors.New("could not match any repository types"))
		return
	}

	if appErr != nil {
		a.ErrorCh <- appErr
		return
	}

	if version.String() == "" {
		a.Logger.Error("app", model.NewInternalAppError("app.crawler", "CrawlSoftware", errors.New("parsed version is empty for '"+software.Repository+"'")))
		return
	}

	// New notification if new version and is not the first crawl for this software
	if severity := model.NewVersion(software.Version).Compare(version); severity != "" {
		if software.Version != "" {
			notificationCh <- &model.Notification{
				Software:   software,
				NewVersion: *version,
			}
		}

		now := time.Now()

		// Update database software version
		software.Version = version.String()
		software.VersionUpdatedAt = &now
		if err := a.Store.UpdateSoftware(&software); err != nil {
			a.ErrorCh <- appErr
			return
		}
	}
}

// DispatchNotifications listen for any new notification and dispatch the notification to users by email
func (a *App) DispatchNotifications() chan *model.Notification {
	notificationCh := make(chan *model.Notification)

	go func() {
		notificationsMap := make(map[uint64]*model.UserNotifications)

		// For each new notification, dispatch it to subscribed users
		for notification := range notificationCh {

			// users, err := a.Store.GetUsersWithSubscriptionTo(notification.Software.ID)
			users, err := a.Store.GetUsers()
			if err != nil {
				a.ErrorCh <- err
			}

			for _, user := range *users {
				if user.EmailVerified && user.Email != "" {

					if notificationsMap[user.ID] == nil {
						notificationsMap[user.ID] = &model.UserNotifications{
							User: &user,
						}
					}

					notificationsMap[user.ID].Notifications = append(notificationsMap[user.ID].Notifications, notification)
				}
			}
		}

		// After closing notification channel
		for _, notifications := range notificationsMap {
			subject, body, err := NewNotificationsEmail(notifications)
			if err != nil {
				a.ErrorCh <- err
			}
			if err := a.EmailClient.Send(notifications.User.Email, subject, body); err != nil {
				a.ErrorCh <- err
			}
		}

	}()

	return notificationCh
}

// CallGet is used to call remote APIs to fetch data
func CallGet(endpoint string) ([]byte, *model.AppError) {
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, model.NewInternalAppError("app.crawler", "CallGet", err)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, model.NewInternalAppError("app.crawler", "CallGet", err)
	}
	defer resp.Body.Close()

	if (resp.StatusCode/100)%2 != 0 {
		return nil, model.NewInternalAppError("app.crawler", "CallGet", errors.New(resp.Status+" while calling '"+endpoint+"'"))
	}

	// Github rate limit
	if resp.Header.Get("X-RateLimit-Remaining") == "0" {
		return nil, model.NewInternalAppError("app.crawler", "CallGet", errors.New("reached rate limit"))
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, model.NewInternalAppError("app.crawler", "CallGet", err)
	}

	return bodyBytes, nil
}
