package app

import (
	"time"

	"github.com/robfig/cron"
	"gitlab.com/vtrack/vtrack-server/model"
)

// Runner structure
type Runner struct {
	Cron *cron.Cron
}

// NewRunner creates a new job runner and returns it into the app
func NewRunner(location string, errorCh chan<- *model.AppError) (*Runner, *model.AppError) {
	l, err := time.LoadLocation(location)
	if err != nil {
		return nil, model.NewInternalAppError("app.runner", "NewRunner", err)
	}

	j := &Runner{
		Cron: cron.NewWithLocation(l),
	}

	j.Cron.Start()
	return j, nil
}

// Close the runner
func (o *Runner) Close() {
	o.Cron.Stop()
}
