package app

import (
	"encoding/json"
	"errors"
	"net/url"

	"gitlab.com/vtrack/vtrack-server/model"
)

const (
	// GithubHostname define the hostname for github urls
	GithubHostname = "github.com"
	// GithubAPIURL define the api URL for github
	GithubAPIURL = "https://api.github.com"
)

// CrawlerGithub is the struct used to crawl github repositories
type CrawlerGithub struct {
	apiURL       string
	clientID     string
	clientSecret string
}

// NewCrawlerGithub create a new crawler for github
func NewCrawlerGithub(clientID string, clientSecret string) Crawler {
	return &CrawlerGithub{
		apiURL:       GithubAPIURL,
		clientID:     clientID,
		clientSecret: clientSecret,
	}
}

// FetchVersion fetches latest version of the specified github repo
func (c *CrawlerGithub) FetchVersion(sourceURL *url.URL) (*model.Version, *model.AppError) {
	var err *model.AppError
	var result string

	if result, err = c.fetchVersionFromReleases(sourceURL); err != nil {
		return nil, err
	}

	if result == "" {
		if result, err = c.fetchVersionFromTags(sourceURL); err != nil {
			return nil, err
		}
	}

	if result == "" {
		return nil, model.NewInternalAppError("app.crawler_github", "FetchVersion", errors.New("could not fetch version for '"+sourceURL.String()+"'"))
	}

	return model.NewVersion(result), nil
}

func (c *CrawlerGithub) fetchVersionFromReleases(sourceURL *url.URL) (string, *model.AppError) {
	bodyBytes, err := CallGet(c.apiURL + "/repos" + sourceURL.Path + "/releases/latest" + c.authQueryString())
	if err != nil {
		return "", err
	}

	body := struct {
		TagName string `json:"tag_name"`
	}{}

	if err := json.Unmarshal(bodyBytes, &body); err != nil {
		return "", model.NewInternalAppError("app.crawler_github", "fetchVersionFromReleases", err)
	}

	return body.TagName, nil
}

func (c *CrawlerGithub) fetchVersionFromTags(sourceURL *url.URL) (string, *model.AppError) {
	bodyBytes, err := CallGet(c.apiURL + "/repos" + sourceURL.Path + "/tags" + c.authQueryString())
	if err != nil {
		return "", err
	}

	body := []struct {
		Name string `json:"name"`
	}{}

	if err := json.Unmarshal(bodyBytes, &body); err != nil {
		return "", model.NewInternalAppError("app.crawler_github", "fetchVersionFromTags", err)
	}

	return body[0].Name, nil
}

func (c *CrawlerGithub) authQueryString() string {
	if c.clientID != "" && c.clientSecret != "" {
		return "?client_id=" + c.clientID + "&client_secret=" + c.clientSecret
	}
	return ""
}
