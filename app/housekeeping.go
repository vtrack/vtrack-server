package app

import "gitlab.com/vtrack/vtrack-server/model"

// Housekeeping clean up databse, reindex database, remove unused data, email unverified users
func (a *App) Housekeeping() *model.AppError {
	return nil
}
