package clog

import (
	"io"
	"log"
	"os"

	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/utils"
)

// LogFilePath path
const LogFilePath = "vtrack.log"

// Logger structure
type Logger struct {
	file *os.File
	log.Logger
}

// New logger
func New() (*Logger, *model.AppError) {
	logger := new(Logger)

	var err error
	if utils.Exist(LogFilePath) {
		logger.file, err = os.OpenFile(LogFilePath, os.O_APPEND|os.O_WRONLY, 0640)
	} else {
		logger.file, err = os.Create(LogFilePath)
	}
	if err != nil {
		return nil, model.NewInternalAppError("clog.clog", "New", err)
	}

	// var w io.Writer
	// if terminal.IsTerminal(int(os.Stdout.Fd())) {
	// 	w = io.MultiWriter(os.Stdout, logger.file)
	// } else {
	// 	w = io.Writer(logger.file)
	// }

	w := io.MultiWriter(os.Stdout, logger.file)

	logger.SetOutput(w)
	logger.SetFlags(log.LstdFlags)
	return logger, nil
}

// Close the logger
func (logger *Logger) Close() *model.AppError {
	if err := logger.file.Close(); err != nil {
		return model.NewInternalAppError("clog.clog", "Close", err)
	}
	return nil
}

// Info print a message
func (logger *Logger) Info(prefix string, i interface{}) {
	logger.Printf("%v.info: %v\n", prefix, i)
}

// Error print an error
func (logger *Logger) Error(prefix string, i interface{}) {
	logger.Printf("%v.error: %v\n", prefix, i)
}

// Fatal print an error and exit
func (logger *Logger) Fatal(prefix string, i interface{}) {
	logger.Printf("%v.fatal: %v\n", prefix, i)
	os.Exit(1)
}
