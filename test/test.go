package test

import (
	"os"
)

func init() {
	if err := os.Chdir(os.Getenv("GOPATH") + "/src/gitlab.com/vtrack/vtrack-server"); err != nil {
		panic(err)
	}
}
