package main

import (
	"gitlab.com/vtrack/vtrack-server/server"
)

//go:generate swag init -g api/api.go

func main() {
	server.Run()
}
