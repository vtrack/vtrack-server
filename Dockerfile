# ############ Build server ############
FROM golang:latest AS server_builder

RUN mkdir -p /go/src/gitlab.com/vtrack/vtrack-server
ADD . /go/src/gitlab.com/vtrack/vtrack-server
WORKDIR /go/src/gitlab.com/vtrack/vtrack-server

RUN go get -u -v "github.com/go-task/task/cmd/task"
RUN go get -v ./...

RUN task server:build

############ Build webapp ############
FROM node:latest AS webapp_builder

RUN git clone https://gitlab.com/vtrack/vtrack-webapp.git /build
WORKDIR /build

RUN npm install
RUN npm run lint
RUN npm run build

############ Build vtrack ############
FROM alpine:latest

RUN apk add --no-cache tzdata
RUN apk add --no-cache ca-certificates

RUN mkdir /app

COPY --from=server_builder /go/src/gitlab.com/vtrack/vtrack-server/vtrack /app/vtrack
RUN chmod a+x /app/vtrack

COPY --from=server_builder /go/src/gitlab.com/vtrack/vtrack-server/default_data.yml /app/default_data.yml
COPY --from=server_builder /go/src/gitlab.com/vtrack/vtrack-server/config.json /app/config.json
COPY --from=server_builder /go/src/gitlab.com/vtrack/vtrack-server/templates /app/templates

RUN mkdir /app/public
COPY --from=webapp_builder /build/dist /app/public/

EXPOSE 4000

WORKDIR /app
CMD ["/app/vtrack"]
