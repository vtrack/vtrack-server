package server

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/vtrack/vtrack-server/api"
	"gitlab.com/vtrack/vtrack-server/app"
	"gitlab.com/vtrack/vtrack-server/clog"
)

// Server structure
type Server struct {
	http.Server
}

// Run the server
func Run() {
	app := app.Init()

	app.Logger.Info("server", "Loading routes")
	router := chi.NewRouter()

	if app.Config.Debug {
		router.Use(middleware.NoCache)
	}

	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)
	router.Use(middleware.StripSlashes)
	router.Use(middleware.Heartbeat("/ping"))

	// router.Use(cors.Default().Handler)
	router.Use(cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodDelete,
			http.MethodGet,
			http.MethodOptions,
			http.MethodPatch,
			http.MethodPost,
			http.MethodPut,
		},
		AllowedHeaders: []string{"*"},
		Debug:          app.Config.Debug,
	}).Handler)

	api.Init(app, router)

	// Static file server
	workDir, _ := os.Getwd()
	publicDir := filepath.Join(workDir, "public")
	FileServer(router, "/", http.Dir(publicDir))

	// chi.Walk(router, func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
	// 	fmt.Printf("%v\t%v\n", method, route)
	// 	return nil
	// })

	srv := &Server{http.Server{
		Addr:         app.Config.ListenAddress,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
		Handler:      router,
	}}

	done := make(chan struct{})
	go func() {
		app.Logger.Info("server", "Server listening at: '"+app.Config.ListenAddress+"'")

		if err := srv.ListenAndServe(); err != nil {
			app.Logger.Error("server", err)
		}

		done <- struct{}{}
	}()

	// Wait for server to shutdown gracefully
	srv.WaitShutdown(app.Logger)
	<-done
	app.Close()
}

// WaitShutdown to avoid a brutal shutdown
func (srv *Server) WaitShutdown(logger *clog.Logger) {
	irqSig := make(chan os.Signal, 1)
	signal.Notify(irqSig, syscall.SIGINT, syscall.SIGTERM)

	// Wait for interuption signal
	<-irqSig
	logger.Info("server", "Stopping the server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err := srv.Shutdown(ctx)
	if err != nil {
		logger.Error("server", err)
	}
}
