package conf

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"github.com/subosito/gotenv"
	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/utils"
)

// ConfigPath path
const ConfigPath = "config.json"

// JWTKeySize key size
const JWTKeySize = 40

var (
	// Version used to pass version at build time
	Version string
	// Commit used to pass latest git commit sha at build time
	Commit string
	// BuildTime used to pass date of build
	BuildTime string
)

// Config structure
type (
	Config struct {
		ApplicationName string `json:"application_name"`
		ListenAddress   string `json:"listen_address"`
		Location        string `json:"location"`

		Database database `json:"database"`
		SMTP     smtp     `json:"smtp"`

		JWTSecret string `json:"jwt_secret"`

		GithubClientID     string `json:"github_client_id"`
		GithubClientSecret string `json:"github_client_secret"`

		Debug bool `json:"debug"`
	}

	database struct {
		Type   string `json:"type"`
		Source string `json:"source"`
	}

	smtp struct {
		Host     string `json:"host"`
		Port     int    `json:"port"`
		Username string `json:"username"`
		Password string `json:"password"`
		From     string `json:"from"`
	}
)

// NewDefaultConfig build the default config
func NewDefaultConfig() *Config {
	return &Config{
		ApplicationName: "vTrack",
		JWTSecret:       utils.RandString(JWTKeySize),
		ListenAddress:   ":4000",
		Database: database{
			Type:   "mysql",
			Source: "root:root@tcp(127.0.0.1:3306)/test?parseTime=true",
		},
	}
}

// Load the config JSON file
func Load() (*Config, *model.AppError) {
	if !utils.Exist(ConfigPath) {
		conf := NewDefaultConfig()
		conf.Save()

		conf.LoadEnv()
		return conf, nil
	}

	var conf Config

	data, err := ioutil.ReadFile(ConfigPath)
	if err != nil {
		return nil, model.NewInternalAppError("conf.conf", "Load", err)
	}

	if err := json.Unmarshal(data, &conf); err != nil {
		return nil, model.NewInternalAppError("conf.conf", "Load", err)
	}

	if conf.JWTSecret == "" {
		conf.JWTSecret = utils.RandString(JWTKeySize)
	}

	conf.LoadEnv()

	return &conf, nil
}

// ToJSON build the JSON body from a struct
func (conf *Config) ToJSON() []byte {
	return model.ToJSON(conf)
}

// Save the config JSON file
func (conf *Config) Save() *model.AppError {
	data, err := json.MarshalIndent(conf, "", "\t")
	if err != nil {
		return model.NewInternalAppError("conf.conf", "Save", err)
	}

	if err := ioutil.WriteFile(ConfigPath, data, 0600); err != nil {
		return model.NewInternalAppError("conf.conf", "Save", err)
	}
	return nil
}

// LoadEnv import any environment variables into the current configuration
func (conf *Config) LoadEnv() {
	gotenv.Load()

	if os.Getenv("APPLICATION_NAME") != "" {
		conf.ApplicationName = os.Getenv("APPLICATION_NAME")
	}

	if os.Getenv("LISTEN_ADDRESS") != "" {
		conf.ListenAddress = os.Getenv("LISTEN_ADDRESS")
	}

	if os.Getenv("LOCATION") != "" {
		conf.Location = os.Getenv("LOCATION")
	}

	if os.Getenv("DATABASE_TYPE") != "" {
		conf.Database.Type = os.Getenv("DATABASE_TYPE")
	}

	if os.Getenv("DATABASE_SOURCE") != "" {
		conf.Database.Source = os.Getenv("DATABASE_SOURCE")
	}

	if os.Getenv("SMTP_HOST") != "" {
		conf.SMTP.Host = os.Getenv("SMTP_HOST")
	}

	if os.Getenv("SMTP_PORT") != "" {
		conf.SMTP.Port = utils.StringToInt(os.Getenv("SMTP_PORT"))
	}

	if os.Getenv("SMTP_USERNAME") != "" {
		conf.SMTP.Username = os.Getenv("SMTP_USERNAME")
	}

	if os.Getenv("SMTP_PASSWORD") != "" {
		conf.SMTP.Password = os.Getenv("SMTP_PASSWORD")
	}

	if os.Getenv("SMTP_FROM") != "" {
		conf.SMTP.From = os.Getenv("SMTP_FROM")
	}

	if os.Getenv("JWT_SECRET") != "" {
		conf.JWTSecret = os.Getenv("JWT_SECRET")
	}

	if os.Getenv("GITHUB_CLIENT_ID") != "" {
		conf.GithubClientID = os.Getenv("GITHUB_CLIENT_ID")
	}

	if os.Getenv("GITHUB_CLIENT_SECRET") != "" {
		conf.GithubClientSecret = os.Getenv("GITHUB_CLIENT_SECRET")
	}

	if os.Getenv("DEBUG") != "" {
		conf.Debug = true
	}
}
