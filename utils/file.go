package utils

import (
	"os"
	"path/filepath"
)

// CreateDir create a directory in the specified path
func CreateDir(path string) error {
	path = filepath.Clean(path)
	if !Exist(path) {
		return os.Mkdir(path, 0750)
	}
	return nil
}

// Exist check wether a file/dir exists
func Exist(path string) bool {
	path = filepath.Clean(path)
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}
