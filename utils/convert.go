package utils

import (
	"strconv"
)

// UInt64ToString convert a uint64 to string
func UInt64ToString(it uint64) string {
	return strconv.FormatUint(it, 10)
}

// IntToString convert a int to string
func IntToString(it int) string {
	return strconv.FormatUint(uint64(it), 10)
}

// StringToUInt64 convert a string to uint64
func StringToUInt64(it string) uint64 {
	if it == "" {
		return 0
	}
	v, err := strconv.ParseUint(it, 10, 64)
	if err != nil {
		panic(err)
	}
	return v
}

// StringToInt convert a string to int
func StringToInt(it string) int {
	if it == "" {
		return 0
	}
	v, err := strconv.ParseInt(it, 10, 32)
	if err != nil {
		panic(err)
	}
	return int(v)
}
