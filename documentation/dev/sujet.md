## Traqueur de version de logiciels

Tuteur / client : Julien BROISIN

#### Description

Lorsque l'on utilise un grand nombre d'applications ou de librairies, la surveillance des éventuelles mises à jour ainsi que des alertes de sécurité est souvent difficile.

L'objectif est de créer une application web multi utilisateurs, qui surveillera la version des logiciels que l'on y a ajouté. Ceci permettra de recevoir une notification des nouvelles mises à jour. Cette application pourra surveiller les sites les plus populaires, tel que Github, Gitlab, Bitbucket, mais aussi des flux RSS ou faire un scan de page HTML.

L'application pourra être découpée en 2 parties, la partie backend qui gère les données et l'authentification, et la partie frontend qui permettra à l'utilisateur d’accéder à ses informations et ses réglages de manière ergonomique.

#### Ressources

##### Backend

- [Golang](https://golang.org/)
- [Java](https://java.com/fr/)
- [PHP](http://php.net/manual/fr/intro-whatis.php)
- [Gin, un framework REST API pour Golang](https://github.com/gin-gonic/gin)
- [Ponzu, un CMS Headless en Golang](https://github.com/ponzu-cms/ponzu)
- [Storm, un ORM (DB) pour Golang](https://github.com/asdine/storm)
- [Lavarel, un framework pour PHP](https://github.com/laravel/laravel)
- [Spring, un framework pour Java](https://spring.io/projects/spring-boot)

##### Frontend

- [Vue.js, un framework Js pour SPA](https://vuejs.org/)
- [Axios, un client http en Js](https://github.com/axios/axios)
- [Boostrap, un framework Css](https://getbootstrap.com/)

##### Gestion du projet

- [Planification avec Trello](https://trello.com/)
- [Travail collaboratif avec Git et un traqueur de bug](https://gitea.io/en-us/)
- [Continous Intergation et Continous Delivery](https://drone.io/)
- [Packaging et déploiement](https://www.docker.com/)
