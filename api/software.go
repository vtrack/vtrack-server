package api

import (
	"net/http"

	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/utils"
)

// GetSoftwares handler
// @Summary GetSoftwares
// @Description Get all softwares
// @Tags Softwares
// @Produce json
//
// @Router /softwares [get]
// @Success 200 {array} model.Software
// @Failure 401 {object} model.AppError
// @Failure 500
func GetSoftwares(w http.ResponseWriter, r *http.Request) {
	data, err := api.Store.GetSoftwares()
	if HasError(w, err) {
		return
	}

	w.Write(data.ToJSON())
}

// AddSoftware handler
// @Summary AddSoftware
// @Description Add a software
// @Description The new ressource Location is returned in the header
// @Tags Softwares
// @Accept json
//
// @Router /softwares [post]
// @Param software body model.SoftwarePatch true "Software patch body"
// @Success 201
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 500
func AddSoftware(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())

	patch, err := model.SoftwarePatchFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	software := patch.NewSoftware(utils.StringToUInt64(claims["id"].(string)))

	if HasError(w, software.IsValid()) {
		return
	}

	softwareID, err := api.Store.SaveSoftware(software)
	if HasError(w, err) {
		return
	}

	w.Header().Set("Location", "/api/v1/softwares/"+utils.UInt64ToString(softwareID))
	w.WriteHeader(http.StatusCreated)
}

// GetSoftware handler
// @Summary GetSoftware
// @Description Get a software
// @Tags Softwares
// @Produce json
//
// @Router /softwares/{softwareID} [get]
// @Param softwareID path int true "Software ID"
// @Success 200 {object} model.Software
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func GetSoftware(w http.ResponseWriter, r *http.Request) {
	software := SoftwareFromCtx(r.Context())
	JSON(w, software.ToJSON())
}

// DeleteSoftware handler
// @Summary DeleteSoftware
// @Description Delete a Software
// @Description [requires admin or ownership permissions]
// @Tags Softwares
//
// @Router /softwares/{softwareID} [delete]
// @Param softwareID path int true "Software ID"
// @Success 204
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func DeleteSoftware(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	software := SoftwareFromCtx(r.Context())

	if software.CreatedBy != ClaimsUserID(claims) && !IsAdmin(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	if HasError(w, api.Store.DeleteSoftware(software.ID)) {
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// PatchSoftware handler
// @Summary PatchSoftware
// @Description Patch a Software
// @Description [requires admin or ownership permissions]
// @Tags Softwares
// @Accept json
// @Produce json
//
// @Router /softwares/{softwareID} [patch]
// @Param softwareID path int true "Software ID"
// @Param software body model.SoftwarePatch true "Software patch body"
// @Success 200 {object} model.Software
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func PatchSoftware(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	software := SoftwareFromCtx(r.Context())

	if software.CreatedBy != ClaimsUserID(claims) && !IsAdmin(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	patch, err := model.SoftwarePatchFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	// TODO: User patch.IsValid when patching, not software.IsValid

	software.FromPatch(patch)
	if HasError(w, software.IsValid()) {
		return
	}

	if HasError(w, api.Store.UpdateSoftware(software)) {
		return
	}

	software, err = api.Store.GetSoftware(software.ID)
	if HasError(w, err) {
		return
	}

	JSON(w, software.ToJSON())
}
