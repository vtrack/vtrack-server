package api

import (
	"context"
	"net/http"
)

// PaginateValues structure
type PaginateValues struct {
	Limit  string
	Offset string
}

// PaginationKey constant
const PaginationKey CtxKey = "pagination"

// Pagination middleware allow to insert pagination as query parameters
func Pagination(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		limit := r.URL.Query().Get("limit")
		offset := r.URL.Query().Get("offset")

		ctx := context.WithValue(r.Context(), PaginationKey, PaginateValues{
			Limit:  limit,
			Offset: offset,
		})

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
