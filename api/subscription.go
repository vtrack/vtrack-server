package api

import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/utils"
)

// GetSubscriptions handler
// @Summary GetSubscriptions
// @Description Get all user subscriptions
// @Description [requires admin or ownership permissions]
// @Tags Subscriptions
// @Produce json
//
// @Router /users/{userID}/subscriptions [get]
// @Param userID path int true "User ID"
// @Success 200 {array} model.Subscription
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func GetSubscriptions(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context())

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	subscriptions, err := api.Store.GetSubscriptions(user.ID)
	if HasError(w, err) {
		return
	}

	w.Write(subscriptions.ToJSON())
}

// AddSubscription handler
// @Summary AddSubscription
// @Description Add a subscription
// @Tags Subscriptions
// @Accept json
//
// @Router /api/v1/users/{userID}/subscriptions/{softwareID} [put]
// @Param userID path int true "User ID"
// @Param softwareID path int true "Software ID"
// @Param subscription body model.SubscriptionPatch true "Subscription patch body"
// @Success 201
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func AddSubscription(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context())
	software := SoftwareFromCtx(r.Context())

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	patch, err := model.SubscriptionPatchFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	subscription := patch.NewSubscription(user.ID, software.ID)
	if HasError(w, subscription.IsValid()) {
		return
	}

	if HasError(w, api.Store.SaveSubscription(user.ID, software.ID, subscription)) {
		return
	}

	w.Header().Set("Location", "/api/v1/users/"+utils.UInt64ToString(user.ID)+"/subscriptions/"+utils.UInt64ToString(software.ID))
	w.WriteHeader(http.StatusCreated)
}

// DeleteSubscription handler
// @Summary DeleteSubscription
// @Description Delete a Subscription
// @Description [requires admin or ownership permissions]
// @Tags Subscriptions
//
// @Router /api/v1/users/{userID}/subscriptions/{softwareID} [delete]
// @Param userID path int true "User ID"
// @Param softwareID path int true "Software ID"
// @Success 204
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func DeleteSubscription(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context())
	software := SoftwareFromCtx(r.Context())

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	_, err := api.Store.GetSubscription(user.ID, software.ID)
	if HasError(w, err) {
		return
	}

	if HasError(w, api.Store.DeleteSubscription(user.ID, software.ID)) {
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// PatchSubscription handler
// @Summary PatchSubscription
// @Description Patch a Subscription
// @Description [requires admin or ownership permissions]
// @Tags Subscriptions
// @Accept json
// @Produce json
//
// @Router /api/v1/users/{userID}/subscriptions/{softwareID} [patch]
// @Param userID path int true "User ID"
// @Param softwareID path int true "Software ID"
// @Param subscription body model.SubscriptionPatch true "Subscription patch body"
// @Success 200 {object} model.Subscription
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func PatchSubscription(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context())
	software := SoftwareFromCtx(r.Context())

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	patch, err := model.SubscriptionPatchFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	subscription, err := api.Store.GetSubscription(user.ID, software.ID)
	if HasError(w, err) {
		return
	}

	subscription.FromPatch(patch)
	if HasError(w, subscription.IsValid()) {
		return
	}

	if HasError(w, api.Store.UpdateSubscription(user.ID, software.ID, subscription)) {
		return
	}

	subscription, err = api.Store.GetSubscription(user.ID, software.ID)
	if HasError(w, err) {
		return
	}

	JSON(w, subscription.ToJSON())
}

// AddSubscriptionTag handler
// @Summary AddSubscriptionTag
// @Description Add a subscription tag
// @Tags Subscriptions
// @Accept json
//
// @Router /api/v1/users/{userID}/subscriptions/{softwareID}/tags/{tagID} [put]
// @Param userID path int true "User ID"
// @Param softwareID path int true "Software ID"
// @Param tagID path int true "Software ID"
// @Success 201
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func AddSubscriptionTag(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context())
	software := SoftwareFromCtx(r.Context())

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	tagID := chi.URLParam(r, "tagID")
	if tagID == "" {
		HasError(w, model.NewAppError("api.subscription", "AddSubscriptionTag", nil, "Missing tag id in path", http.StatusBadRequest))
		return
	}

	tag, err := api.Store.GetUserTag(user.ID, utils.StringToUInt64(tagID))
	if HasError(w, err) {
		return
	}

	if HasError(w, api.Store.SaveSubscriptionTag(user.ID, software.ID, tag.ID)) {
		return
	}

	w.Header().Set("Location", "/api/v1/users/"+utils.UInt64ToString(user.ID)+"/subscriptions/"+utils.UInt64ToString(software.ID)+"/tags/"+utils.UInt64ToString(tag.ID))
	w.WriteHeader(http.StatusCreated)
}

// DeleteSubscriptionTag handler
// @Summary DeleteSubscriptionTag
// @Description Delete a subscription tag
// @Tags Subscriptions
// @Accept json
//
// @Router /api/v1/users/{userID}/subscriptions/{softwareID}/tags/{tagID} [delete]
// @Param userID path int true "User ID"
// @Param softwareID path int true "Software ID"
// @Param tagID path int true "Software ID"
// @Success 204
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func DeleteSubscriptionTag(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context())
	software := SoftwareFromCtx(r.Context())

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	tagID := chi.URLParam(r, "tagID")
	if tagID == "" {
		HasError(w, model.NewAppError("api.subscription", "AddSubscriptionTag", nil, "Missing tag id in path", http.StatusBadRequest))
		return
	}

	tag, err := api.Store.GetUserTag(user.ID, utils.StringToUInt64(tagID))
	if HasError(w, err) {
		return
	}

	if HasError(w, api.Store.DeleteSubscriptionTag(user.ID, software.ID, tag.ID)) {
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
