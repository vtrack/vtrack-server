package api

import (
	"net/http"

	"gitlab.com/vtrack/vtrack-server/model"
)

// GetCategories handler
// @Summary GetCategories
// @Description Get all categories
// @Tags Categories
// @Produce json
//
// @Router /categories [get]
// @Success 200 {array} model.Category
// @Failure 401 {object} model.AppError
// @Failure 500
func GetCategories(w http.ResponseWriter, r *http.Request) {
	data, err := api.Store.GetCategories()
	if HasError(w, err) {
		return
	}

	JSON(w, data.ToJSON())
}

// AddCategory handler
// @Summary AddCategory
// @Description Add a category
// @Description The new ressource Location is returned in the header
// @Description [requires admin]
// @Tags Categories
// @Accept json
//
// @Router /categories [post]
// @Param category body model.Category true "Category body"
// @Success 201
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func AddCategory(w http.ResponseWriter, r *http.Request) {
	data, err := model.CategoryFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	if HasError(w, api.Store.SaveCategory(data)) {
		return
	}

	w.Header().Set("Location", "/api/v1/categories/"+data.Name)
	w.WriteHeader(http.StatusCreated)
}

// DeleteCategory handler
// @Summary DeleteCategory
// @Description Delete a category
// @Description [requires admin]
// @Tags Categories
//
// @Router /categories/{categoryName} [delete]
// @Param categoryName path string true "Category Name"
// @Success 204
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func DeleteCategory(w http.ResponseWriter, r *http.Request) {
	data := r.Context().Value(categoryCtxKey).(string)

	if HasError(w, api.Store.DeleteCategory(data)) {
		return
	}

	w.WriteHeader(http.StatusOK)
}
