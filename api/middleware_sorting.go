package api

import (
	"context"
	"net/http"
)

// SortingKey constant
const SortingKey CtxKey = "sorting"

// SortingValues structure
type SortingValues struct {
	SortBy  string
	OrderBy string
}

// Sorting middleware allow to insert sorting as query parameters
func Sorting(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sortBy := r.URL.Query().Get("sort_by")
		orderBy := r.URL.Query().Get("order_by")

		ctx := context.WithValue(r.Context(), SortingKey, SortingValues{
			SortBy:  sortBy,
			OrderBy: orderBy,
		})

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
