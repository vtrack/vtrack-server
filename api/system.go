package api

import "net/http"

// GetInfo handler
// @Summary GetInfo
// @Description Get application informations
// @Tags System
// @Produce json
//
// @Router /info [get]
// @Success 200 {object} model.Info
// @Failure 500
func GetInfo(w http.ResponseWriter, r *http.Request) {}

// CrawlSoftwares handler
// @Summary CrawlSoftwares
// @Description Crawl softwares manually
// @Tags System
// @Produce json
//
// @Router /system/crawl [get]
// @Success 202
// @Failure 500
func CrawlSoftwares(w http.ResponseWriter, r *http.Request) {
	// TODO: Prevent from accessing admin utils
	// claims := ClaimsFromCtx(r.Context())
	//
	// if !IsAdmin(claims) {
	// 	w.WriteHeader(http.StatusForbidden)
	// 	return
	// }

	if !api.IsRunning.Crawler {
		api.Logger.Info("api", "Start crawling softwares job")
		go api.CrawlSoftwares()
	}
	w.WriteHeader(http.StatusAccepted)
}
