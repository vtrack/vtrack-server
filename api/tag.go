package api

import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/utils"
)

// GetUserTags handler
// @Summary GetUserTags
// @Description Get all user tags
// @Description [requires admin or ownership permissions]
// @Tags Tags
// @Produce json
//
// @Router /users/{userID}/tags [get]
// @Param userID path int true "User ID"
// @Success 200 {array} model.Tag
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func GetUserTags(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context()) // TODO: move permissions checking before middleware (security issue !)

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	tags, err := api.Store.GetUserTags(user.ID)
	if HasError(w, err) {
		return
	}

	JSON(w, tags.ToJSON())
}

// AddUserTag handler
// @Summary AddUserTag
// @Description Add a user tag
// @Description The new ressource Location is returned in the header
// @Tags Tags
// @Accept json
//
// @Router /users/{userID}/tags [post]
// @Param userID path int true "User ID"
// @Param tag body model.TagPatch true "Tag patch body"
// @Success 201
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func AddUserTag(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context()) // TODO: move permissions checking before middleware (security issue !)

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	tag, err := model.TagFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	if HasError(w, tag.IsValid()) {
		return
	}

	tagID, err := api.Store.SaveUserTag(user.ID, tag)
	if HasError(w, err) {
		return
	}

	w.Header().Set("Location", "/api/v1/users/"+utils.UInt64ToString(user.ID)+"/tags/"+utils.UInt64ToString(tagID))
	w.WriteHeader(http.StatusCreated)
}

// DeleteUserTag handler
// @Summary DeleteUserTag
// @Description Delete a tag
// @Description [requires admin or ownership permissions]
// @Tags Tags
//
// @Router /users/{userID}/tags/{tagID} [delete]
// @Param userID path int true "User ID"
// @Param tagID path int true "Tag ID"
// @Success 204
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func DeleteUserTag(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context()) // TODO: move permissions checking before middleware (security issue !)

	tagID := chi.URLParam(r, "tagID")
	if tagID == "" {
		HasError(w, model.NewAppError("api.tag", "DeleteUserTag", nil, "Missing tag id in path", http.StatusBadRequest))
		return
	}

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	tag, err := api.Store.GetUserTag(user.ID, utils.StringToUInt64(tagID))
	if HasError(w, err) {
		return
	}

	if HasError(w, api.Store.DeleteUserTag(user.ID, tag.ID)) {
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// PatchUserTag handler
// @Summary PatchUserTag
// @Description Patch a tag
// @Description [requires admin or ownership permissions]
// @Tags Tags
// @Accept json
// @Produce json
//
// @Router /users/{userID}/tags/{tagID} [patch]
// @Param userID path int true "User ID"
// @Param tagID path int true "Tag ID"
// @Param tag body model.TagPatch true "Tag patch body"
// @Success 200 {object} model.Tag
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func PatchUserTag(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context()) // TODO: move permissions checking before middleware (security issue !)

	tagID := chi.URLParam(r, "tagID")
	if tagID == "" {
		HasError(w, model.NewAppError("api.tag", "DeleteUserTag", nil, "Missing tag id in path", http.StatusBadRequest))
		return
	}

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	patch, err := model.TagPatchFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	tag, err := api.Store.GetUserTag(user.ID, utils.StringToUInt64(tagID))
	if HasError(w, err) {
		return
	}

	tag.FromPatch(patch)

	if HasError(w, api.Store.UpdateUserTag(user.ID, tag)) {
		return
	}

	tag, err = api.Store.GetUserTag(user.ID, utils.StringToUInt64(tagID))
	if HasError(w, err) {
		return
	}

	JSON(w, tag.ToJSON())
}
