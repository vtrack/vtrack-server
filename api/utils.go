package api

import (
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/utils"
)

// HasError check wether an error occured and write it to response writer
func HasError(w http.ResponseWriter, err *model.AppError) bool {
	if err != nil {

		if err.StatusCode == http.StatusInternalServerError || api.Config.Debug {
			api.Logger.Error("api", err)
		}

		w.WriteHeader(err.StatusCode)
		JSON(w, err.ToJSON())
		return true
	}
	return false
}

// JSON write data to reponse writer and set the right content type
func JSON(w http.ResponseWriter, data []byte) {
	SetContentTypeJSON(w)
	w.Write(data)
}

// Plain write data to reponse writer and set the right content type
func Plain(w http.ResponseWriter, data []byte) {
	SetContentTypePlain(w)
	w.Write(data)
}

// SetContentTypePlain set the content type to text plain
func SetContentTypePlain(w http.ResponseWriter) {
	w.Header().Set("Content-type", "text/plain; charset=utf-8")
}

// SetContentTypeJSON set the content type to application json
func SetContentTypeJSON(w http.ResponseWriter) {
	w.Header().Set("Content-type", "application/json; charset=utf-8")
}

// IsAdmin check wether jwt token has admin role
func IsAdmin(claims jwt.MapClaims) bool {
	return model.Roles(claims["roles"].(string)).HasRole(model.RoleAdmin)
}

// ClaimsUserID extract the user id from the jwt token
func ClaimsUserID(claims jwt.MapClaims) uint64 {
	return utils.StringToUInt64(claims["id"].(string))
}
