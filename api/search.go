package api

import (
	"net/http"
)

// Search handler
// @Summary Search
// @Description Search for softwares
// @Tags Softwares
// @Produce json
//
// @Router /search [get]
// @Param q query string true "Search query"
// @Success 200 {array} model.Software
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 500
func Search(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("q")

	if query == "" {
		softwares, err := api.Store.GetSoftwares()
		if HasError(w, err) {
			return
		}

		JSON(w, softwares.ToJSON())
		return
	}

	softwares, err := api.Store.SearchSoftwares(query)
	if HasError(w, err) {
		return
	}

	JSON(w, softwares.ToJSON())
}
