package api

import (
	"context"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/utils"
)

const (
	userCtxKey     CtxKey = "user"
	categoryCtxKey CtxKey = "category"
	softwareCtxKey CtxKey = "software"
)

// ClaimsFromCtx get data from context
func ClaimsFromCtx(ctx context.Context) jwt.MapClaims {
	_, claims, _ := jwtauth.FromContext(ctx)
	return claims
}

// UserCtx middleware
func UserCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userID := chi.URLParam(r, "userID")
		if userID == "" {
			HasError(w, model.NewAppError("api.middleware_context", "UserCtx", nil, "Missing user id in path", http.StatusBadRequest))
			return
		}

		data, err := api.Store.GetUser(utils.StringToUInt64(userID))
		if HasError(w, err) {
			return
		}

		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), userCtxKey, data)))
	})
}

// UserFromCtx get data from context
func UserFromCtx(ctx context.Context) *model.User {
	return ctx.Value(userCtxKey).(*model.User)
}

// CategoryCtx middleware
func CategoryCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		categoryName := chi.URLParam(r, "categoryName")
		if categoryName == "" {
			HasError(w, model.NewAppError("api.middleware_context", "CategoryCtx", nil, "Missing category name in path", http.StatusBadRequest))
			return
		}

		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), categoryCtxKey, categoryName)))
	})
}

// CategoryFromCtx get data from context
func CategoryFromCtx(ctx context.Context) *model.Category {
	return ctx.Value(categoryCtxKey).(*model.Category)
}

// SoftwareCtx middleware
func SoftwareCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		softwareID := chi.URLParam(r, "softwareID")
		if softwareID == "" {
			HasError(w, model.NewAppError("api.middleware_context", "SoftwareCtx", nil, "Missing software id in path", http.StatusBadRequest))
			return
		}

		data, err := api.Store.GetSoftware(utils.StringToUInt64(softwareID))
		if HasError(w, err) {
			return
		}

		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), softwareCtxKey, data)))
	})
}

// SoftwareFromCtx get data from context
func SoftwareFromCtx(ctx context.Context) *model.Software {
	return ctx.Value(softwareCtxKey).(*model.Software)
}
