package api

import (
	"net/http"

	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/utils"
)

// GetUsers handler
// @Summary GetUsers
// @Description Get all users
// @Description [requires admin]
// @Tags Users
// @Produce json
//
// @Router /users [get]
// @Success 200 {array} model.User
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func GetUsers(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())

	if !IsAdmin(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	data, err := api.Store.GetUsers()
	if HasError(w, err) {
		return
	}

	JSON(w, data.ToJSON())
}

// AddUser handler
// @Summary AddUser
// @Description Add a user
// @Description The new ressource Location is returned in the header
// @Description [requires admin]
// @Tags Users
// @Accept json
//
// @Router /users [post]
// @Param user body model.UserPatch true "User patch body"
// @Success 201
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func AddUser(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())

	if !IsAdmin(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	patch, err := model.UserPatchFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	user := patch.NewUser()
	if HasError(w, user.IsValid()) {
		return
	}

	id, err := api.Store.SaveUser(user)
	if HasError(w, err) {
		return
	}

	w.Header().Set("Location", "/api/v1/users/"+utils.UInt64ToString(id))
	w.WriteHeader(http.StatusCreated)
}

// GetUser handler
// @Summary GetUser
// @Description Get a user
// @Description [requires admin or ownership permissions]
// @Tags Users
// @Produce json
//
// @Router /users/{userID} [get]
// @Param userID path int true "User ID"
// @Success 200 {object} model.User
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func GetUser(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context()) // TODO: move permissions checking before middleware (security issue !)

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	JSON(w, user.ToJSON())
}

// DeleteUser handler
// @Summary DeleteUser
// @Description Delete a user
// @Description [requires admin or ownership permissions]
// @Tags Users
//
// @Router /users/{userID} [delete]
// @Param userID path int true "User ID"
// @Success 204
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context()) // TODO: move permissions checking before middleware (security issue !)

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	if HasError(w, api.Store.DeleteUser(user.ID)) {
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// PatchUser handler
// @Summary PatchUser
// @Description Patch a user
// @Description [requires admin or ownership permissions]
// @Tags Users
// @Accept json
// @Produce json
//
// @Router /users/{userID} [patch]
// @Param userID path int true "User ID"
// @Param user body model.UserPatch true "User patch body"
// @Success 200 {object} model.User
// @Failure 400 {object} model.AppError
// @Failure 401 {object} model.AppError
// @Failure 403 {object} model.AppError
// @Failure 500
func PatchUser(w http.ResponseWriter, r *http.Request) {
	claims := ClaimsFromCtx(r.Context())
	user := UserFromCtx(r.Context()) // TODO: move permissions checking before middleware (security issue !)

	if !IsAdmin(claims) && user.ID != ClaimsUserID(claims) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	patch, err := model.UserPatchFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	if HasError(w, patch.IsValid()) {
		return
	}

	user.FromPatch(patch)
	if HasError(w, api.Store.UpdateUser(user)) {
		return
	}

	user, err = api.Store.GetUser(user.ID)
	if HasError(w, err) {
		return
	}

	JSON(w, user.ToJSON())
}
