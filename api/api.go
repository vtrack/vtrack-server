package api

// @title vtrack REST API
// @version 0.1.0
// @description vtrack is a software version tracking server.

// @license.name GPL-3.0
// @license.url https://www.gnu.org/licenses/gpl-3.0.txt

// @host localhost:4000
// @BasePath /api/v1

// @securityDefinitions.apikey JWTAuthorizationBearer
// @in header
// @name Authorization

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"gitlab.com/vtrack/vtrack-server/app"
)

// API structure
type API struct{ *app.App }

var api *API

// Init a new API and build api routes
func Init(a *app.App, root chi.Router) {
	api = &API{a}

	jwtToken = jwtauth.New("HS256", []byte(api.Config.JWTSecret), nil)

	root.Route("/api/v1", func(r chi.Router) {

		r.Get("/info", GetInfo)       // GET		/api/v1/info
		r.Post("/auth", Auth)         // POST		/api/v1/auth
		r.Post("/register", Register) // POST		/api/v1/register

		r.Route("/system", func(r chi.Router) {
			r.Get("/crawl", CrawlSoftwares) // GET		/api/v1/system/crawl
		})

		r.Route("/", func(r chi.Router) {
			r.Use(jwtauth.Verifier(jwtToken))
			r.Use(jwtauth.Authenticator)

			r.Route("/users", func(r chi.Router) {

				r.With(Pagination).With(Sorting).Get("/", GetUsers) // GET		/api/v1/users
				r.Post("/", AddUser)                                // POST		/api/v1/users

				r.Route("/{userID}", func(r chi.Router) {
					r.Use(UserCtx)
					r.Get("/", GetUser)       // GET			/api/v1/users/{userID}
					r.Patch("/", PatchUser)   // PATCH		/api/v1/users/{userID}
					r.Delete("/", DeleteUser) // DELETE		/api/v1/users/{userID}

					r.Route("/tags", func(r chi.Router) {
						r.With(Pagination).With(Sorting).Get("/", GetUserTags) // GET		/api/v1/users/{userID}/tags
						r.Post("/", AddUserTag)                                // POST	/api/v1/users/{userID}/tags

						r.Route("/{tagID}", func(r chi.Router) {
							r.Patch("/", PatchUserTag)   // PATCH		/api/v1/users/{userID}/tags/{tagID}
							r.Delete("/", DeleteUserTag) // DELETE	/api/v1/users/{userID}/tags/{tagID}
						})
					})

					r.Route("/subscriptions", func(r chi.Router) {
						r.With(Pagination).With(Sorting).Get("/", GetSubscriptions) // GET		/api/v1/users/{userID}/subscriptions

						r.Route("/{softwareID}", func(r chi.Router) {
							r.Use(SoftwareCtx)
							r.Put("/", AddSubscription)       // PUT		/api/v1/users/{userID}/subscriptions/{softwareID}
							r.Patch("/", PatchSubscription)   // PATCH	/api/v1/users/{userID}/subscriptions/{softwareID}
							r.Delete("/", DeleteSubscription) // DELETE	/api/v1/users/{userID}/subscriptions/{softwareID}
						})

						r.Route("/tags", func(r chi.Router) {
							r.Put("/", AddSubscriptionTag) // PUT	/api/v1/users/{userID}/subscriptions/{softwareID}/tags/{tagID}

							r.Route("/{tagID}", func(r chi.Router) {
								r.Delete("/", DeleteSubscriptionTag) // DELETE	/api/v1/users/{userID}/subscriptions/{softwareID}/tags/{tagID}
							})
						})

					})
				})
			})

			r.Get("/search", Search) // GET			/api/v1/search

			r.Route("/softwares", func(r chi.Router) {
				r.With(Pagination).With(Sorting).Get("/", GetSoftwares) // GET		/api/v1/softwares
				r.Post("/", AddSoftware)                                // POST	/api/v1/softwares

				r.Route("/{softwareID}", func(r chi.Router) {
					r.Use(SoftwareCtx)
					r.Get("/", GetSoftware)       // GET			/api/v1/softwares/{softwareID}
					r.Patch("/", PatchSoftware)   // PATCH		/api/v1/softwares/{softwareID}
					r.Delete("/", DeleteSoftware) // DELETE	/api/v1/softwares/{softwareID}
				})
			})

			r.Route("/categories", func(r chi.Router) {
				r.With(Pagination).With(Sorting).Get("/", GetCategories) // GET			/api/v1/categories
				r.Post("/", AddCategory)                                 // POST		/api/v1/categories

				r.Route("/{categoryName}", func(r chi.Router) {
					r.Use(CategoryCtx)
					r.Delete("/", DeleteCategory) // DELETE	/api/v1/categories/{categoryName}
				})
			})
		})
	})
}
