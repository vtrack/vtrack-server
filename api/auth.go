package api

import (
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"
	"gitlab.com/vtrack/vtrack-server/model"
	"gitlab.com/vtrack/vtrack-server/utils"
)

var jwtToken *jwtauth.JWTAuth

// Auth handler
// @Summary Auth
// @Description Auth with user auth body
// @Tags Auth
// @Accept json
// @Produce json
//
// @Router /auth [post]
// @Param auth body model.Auth true "Auth body"
// @Success 200 {object} model.Token
// @Failure 401 {object} model.AppError
// @Failure 500
func Auth(w http.ResponseWriter, r *http.Request) {
	auth, err := model.AuthFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	if HasError(w, auth.IsValid()) {
		return
	}

	user, err := api.Store.GetUserByName(auth.Username)
	if user == nil && err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if HasError(w, err) {
		return
	}

	if !model.ComparePassword(user.PasswordHash, auth.Password) {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	_, tokenString, _ := jwtToken.Encode(jwt.MapClaims{
		"id":       utils.UInt64ToString(user.ID),
		"username": user.Username,
		"email":    user.Email,
		"roles":    user.Roles,
	})

	w.Write(model.NewJSONToken(tokenString))
}

// Register handler
// @Summary Register
// @Description Register with user registration body
// @Tags Auth
// @Accept json
//
// @Router /register [post]
// @Param registration body model.Registration true "Registration body"
// @Success 201
// @Failure 400 {object} model.AppError
// @Failure 500
func Register(w http.ResponseWriter, r *http.Request) {
	registration, err := model.RegistrationFromJSON(r.Body)
	if HasError(w, err) {
		return
	}

	if HasError(w, registration.IsValid()) {
		return
	}

	user := registration.NewUser()
	if HasError(w, user.IsValid()) {
		return
	}

	_, err = api.Store.SaveUser(user)
	if HasError(w, err) {
		return
	}

	// TODO: SEND COMFIRMATION EMAIL

	w.WriteHeader(http.StatusCreated)
}
