# vtrack-server

## Usage

You need to install [go-task](https://github.com/go-task/task) wich will help you build or run the application:

Using go:
```sh
go get -u -v github.com/go-task/task/cmd/task
```

Using an install script:
```sh
curl -s https://taskfile.org/install.sh | sh
```

### Run test, lint and more

To run lint, vet and tests use the following command:
```sh
task
```

### Build the server binary

```sh
task server:build
```

### Build the docker image

The created docker image will embed the server binary and the webapp files

```sh
task docker:build
```

### Clean the project folder

```sh
task clean
```
