package model

import "testing"

func TestRoles(t *testing.T) {
	roles := NewRoles(RoleAdmin, "moderator")
	if string(roles) != RoleAdmin+":moderator" {
		t.Errorf("expected: %s, found: %s", RoleAdmin+":moderator", string(roles))
	}
	if !roles.HasRole(RoleAdmin) {
		t.Fail()
	}
	if roles.HasRole("test_role") {
		t.Fail()
	}
}
