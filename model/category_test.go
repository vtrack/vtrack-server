package model

import (
	"strings"
	"testing"
)

func TestCategory(t *testing.T) {
	for _, v := range [][2]string{
		{`{"name":"Web"}`, `{"name":"web"}`},
		{`{"name":"audio"}`, `{"name":"audio"}`},
	} {
		data, err := CategoryFromJSON(strings.NewReader(v[0]))
		if err != nil {
			t.Error(err)
		}

		if err := data.PreSave(); err != nil {
			t.Error(err)
		}

		if string(data.ToJSON()) != v[1] {
			t.Fail()
		}
	}
}
