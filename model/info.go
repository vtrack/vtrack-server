package model

// Info structure
type Info struct {
	ApplicationName      string `json:"application_name"`
	ApplicationVersion   string `json:"application_version"`
	ApplicationCommit    string `json:"application_commit"`
	ApplicationBuildTime string `json:"application_build_time"`
}

// ToJSON build the JSON body from a struct
func (o *Info) ToJSON() []byte {
	return ToJSON(o)
}
