package model

import (
	"testing"
)

func TestVersion(t *testing.T) {
	for _, tt := range []struct {
		S      string
		Result Version
	}{
		{"3.2.1", Version{"3", "2", "1", ""}},
		{"3.2", Version{"3", "2", "", ""}},
		{"3", Version{"3", "", "", ""}},
		{"v3.2.1", Version{"3", "2", "1", ""}},
		{"v3.2", Version{"3", "2", "", ""}},
		{"v3", Version{"3", "", "", ""}},
		{"something3.2.1-rc1", Version{"3", "2", "1", ""}},
		{"something3.2-rc1", Version{"3", "2", "", ""}},
		{"something3-rc1", Version{"3", "", "", ""}},
	} {
		v := NewVersion(tt.S)
		if v.Major != tt.Result.Major ||
			v.Minor != tt.Result.Minor ||
			v.Patch != tt.Result.Patch {
			t.Errorf("with: %s, expected: %s, found: %s", tt.S, tt.Result, v)
		}
	}
}
