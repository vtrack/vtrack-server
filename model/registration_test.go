package model

import (
	"strings"
	"testing"
)

func TestRegistration(t *testing.T) {
	data, err := RegistrationFromJSON(strings.NewReader(`{"username":"username","email":"username@email.com","email_confirmation":"username@email.com","password":"Password123","password_confirmation":"Password123"}`))
	if err != nil {
		t.Error(err)
	}

	if err := data.IsValid(); err != nil {
		t.Error(err)
	}
}
