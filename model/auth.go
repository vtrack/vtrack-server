package model

import (
	"io"
	"net/http"
)

// NewJSONToken return a json body with the jwt token
func NewJSONToken(s string) []byte {
	return ToJSON(&Token{s})
}

// Token structure when passing the JWT
type Token struct {
	Token string `json:"token"`
}

// Auth structure
type Auth struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// AuthFromJSON build a auth body from a JSON body
func AuthFromJSON(data io.Reader) (*Auth, *AppError) {
	var o Auth
	err := FromJSON(&o, data)
	return &o, err
}

// IsValid return wether the registration data is valid
func (o *Auth) IsValid() *AppError {
	if o.Username == "" {
		return NewAppError("model.auth", "IsValid", nil, "Username is empty", http.StatusBadRequest)
	}
	if o.Password == "" {
		return NewAppError("model.auth", "IsValid", nil, "Password is empty", http.StatusBadRequest)
	}
	return nil
}
