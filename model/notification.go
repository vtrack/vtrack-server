package model

// Notification is composed of a software, a new version and its severity
type Notification struct {
	Software   Software
	NewVersion Version
}

// UserNotifications is composed of a user and a list of notifications
type UserNotifications struct {
	User          *User
	Notifications []*Notification
}
