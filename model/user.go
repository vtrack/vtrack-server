package model

import (
	"io"
	"regexp"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

// User structure
type User struct {
	Model

	ID            uint64 `json:"id,omitempty" db:"id"`
	Username      string `json:"username" db:"username"`
	Email         string `json:"email" db:"email"`
	EmailVerified bool   `json:"email_verified" db:"email_verified"`
	Password      string `json:"-,omitempty" db:"-"`
	PasswordHash  string `json:"-" db:"password_hash"`
	Roles         Roles  `json:"roles" db:"roles"`
}

// ToJSON build the JSON body from a struct
func (o *User) ToJSON() []byte {
	return ToJSON(o)
}

var validUsername = regexp.MustCompile(`^[a-zA-Z0-9\.\-_]+$`)

// IsValid return wether the user data is valid
func (o *User) IsValid() *AppError {
	if err := IsValidUsername(o.Username); err != nil {
		return err
	}
	if err := IsValidEmail(o.Email); err != nil {
		return err
	}
	if err := IsValidPassword(o.Password); err != nil {
		return err
	}
	return nil
}

// PreSave clean object before saving to store
func (o *User) PreSave() *AppError {
	o.Email = strings.ToLower(o.Email)
	o.Username = strings.ToLower(o.Username)

	// TODO: Add email confirmation logic
	o.EmailVerified = true

	hash, err := HashPassword(o.Password)
	if err != nil {
		return NewInternalAppError("model.user", "PreSave", err)
	}
	o.PasswordHash = hash

	return nil
}

// FromPatch merge patch user into current user
func (o *User) FromPatch(patch *UserPatch) {
	if patch.Email != nil {
		o.Email = *patch.Email
	}
	if patch.Username != nil {
		o.Username = *patch.Username
	}
	if patch.Password != nil {
		o.Password = *patch.Password
	}
}

// UserFromJSON build a user from a JSON body
func UserFromJSON(data io.Reader) (*User, *AppError) {
	var o User
	err := FromJSON(&o, data)
	return &o, err
}

// Users structure
type Users []User

// ToJSON build the JSON body from the struct
func (o *Users) ToJSON() []byte {
	return ToJSON(o)
}

// UserPatch structure
type UserPatch struct {
	Username *string `json:"name,omitempty"`
	Email    *string `json:"email,omitempty"`
	Password *string `json:"password,omitempty"`
}

// UserPatchFromJSON build a user from a JSON body
func UserPatchFromJSON(data io.Reader) (*UserPatch, *AppError) {
	var o UserPatch
	err := FromJSON(&o, data)
	return &o, err
}

// IsValid return wether the user patch data is valid
func (o *UserPatch) IsValid() *AppError {
	if o.Email != nil {
		if err := IsValidEmail(*o.Email); err != nil {
			return err
		}
	}
	if o.Username != nil {
		if err := IsValidUsername(*o.Username); err != nil {
			return err
		}
	}
	if o.Password != nil {
		if err := IsValidPassword(*o.Password); err != nil {
			return err
		}
	}
	return nil
}

// NewUser return a new user based on patch data
func (o *UserPatch) NewUser() *User {
	return &User{
		Username: *o.Username,
		Email:    *o.Email,
		Password: *o.Password,
	}
}

// HashPassword generates a hash using the bcrypt.GenerateFromPassword
func HashPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

// ComparePassword compares the hash to a plaintext password
func ComparePassword(hash string, password string) bool {
	if len(password) == 0 || len(hash) == 0 {
		return false
	}
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
