package model

import (
	"encoding/json"
	"io"
	"net/http"
	"time"
)

// Model structure
type Model struct {
	CreatedAt time.Time `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at,omitempty" db:"updated_at"`
}

// ToJSON build the JSON body from a struct
func ToJSON(o interface{}) []byte {
	data, err := json.Marshal(o)
	if err != nil {
		return nil
	}
	return data
}

// FromJSON build a structure from a JSON body
func FromJSON(o interface{}, data io.Reader) *AppError {
	if err := json.NewDecoder(data).Decode(&o); err != nil {
		return NewAppError("model.model", "FromJSON", err, "Malformed JSON body", http.StatusBadRequest)
	}
	return nil
}
