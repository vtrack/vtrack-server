package model

import "io"

// Tag structure
type Tag struct {
	Model

	ID        uint64 `json:"id" db:"id"`
	UserID    uint64 `json:"user_id" db:"appuser_id"`
	Name      string `json:"name" db:"name"`
	Color     string `json:"color" db:"color"`
	TextLight bool   `json:"text_light" db:"text_light"`
}

// ToJSON build the JSON body from a struct
func (o *Tag) ToJSON() []byte {
	return ToJSON(o)
}

// TagFromJSON build a user from a JSON body
func TagFromJSON(data io.Reader) (*Tag, *AppError) {
	var o Tag
	err := FromJSON(&o, data)
	return &o, err
}

// IsValid return wether the tag data is valid
func (o *Tag) IsValid() *AppError {
	return nil
}

// FromPatch merge patch user into current user
func (o *Tag) FromPatch(patch *TagPatch) {
	if patch.Name != nil {
		o.Name = *patch.Name
	}
	if patch.Color != nil {
		o.Color = *patch.Color
	}
	if patch.TextLight != nil {
		o.TextLight = *patch.TextLight
	}
}

// Tags structure
type Tags []Tag

// ToJSON build the JSON body from the struct
func (o *Tags) ToJSON() []byte {
	return ToJSON(o)
}

// TagPatch structure
type TagPatch struct {
	Name      *string `json:"name,omitempty"`
	Color     *string `json:"color,omitempty"`
	TextLight *bool   `json:"text_light,omitempty"`
}

// TagPatchFromJSON build a user from a JSON body
func TagPatchFromJSON(data io.Reader) (*TagPatch, *AppError) {
	var o TagPatch
	err := FromJSON(&o, data)
	return &o, err
}
