package model

import (
	"io"
	"net/http"
	"time"
)

// Software structure
type Software struct {
	Model
	ID               uint64     `json:"id,omitempty" db:"id"`
	Name             string     `json:"name" db:"name"`
	Description      string     `json:"description" db:"description"`
	Repository       string     `json:"repository" db:"repository"`
	RepositoryType   string     `json:"repository_type" db:"repository_type"`
	Website          string     `json:"website" db:"website"`
	Version          string     `json:"version" db:"version"`
	VersionUpdatedAt *time.Time `json:"version_updated_at" db:"version_updated_at"`
	CreatedBy        uint64     `json:"created_by" db:"created_by"`
}

// ToJSON build the JSON body from a struct
func (o *Software) ToJSON() []byte {
	return ToJSON(o)
}

// IsValid return wether the software data is valid
func (o *Software) IsValid() *AppError {
	if o.Name == "" {
		return NewAppError("model.software", "IsValid", nil, "empty name", http.StatusBadRequest)
	}
	if o.RepositoryType == "" {
		return NewAppError("model.software", "IsValid", nil, "empty repository type", http.StatusBadRequest)
	}
	if o.Repository == "" {
		return NewAppError("model.software", "IsValid", nil, "empty repository", http.StatusBadRequest)
	}

	ok := false
	for _, repositoryType := range AllowedRepositoryTypes {
		if o.RepositoryType == repositoryType {
			ok = true
			break
		}
	}
	if !ok {
		return NewAppError("model.software", "IsValid", nil, "Repository type is invalid", http.StatusBadRequest)
	}

	return nil
}

// FromPatch merge patch user into current user
func (o *Software) FromPatch(patch *SoftwarePatch) {
	// Reset version after changing repository type or url
	if patch.Repository != nil || patch.RepositoryType != nil {
		o.Version = ""
		o.VersionUpdatedAt = nil
	}
	if patch.Description != nil {
		o.Description = *patch.Description
	}
	if patch.Name != nil {
		o.Name = *patch.Name
	}
	if patch.Repository != nil {
		o.Repository = *patch.Repository
	}
	if patch.RepositoryType != nil {
		o.RepositoryType = *patch.RepositoryType
	}
	if patch.Website != nil {
		o.Website = *patch.Website
	}
}

// SoftwareFromJSON build a user from a JSON body
func SoftwareFromJSON(data io.Reader) (*Software, *AppError) {
	var o Software
	err := FromJSON(&o, data)
	return &o, err
}

// Softwares structure
type Softwares []Software

// ToJSON build the JSON body from the struct
func (o *Softwares) ToJSON() []byte {
	return ToJSON(o)
}

// SoftwarePatch structure
type SoftwarePatch struct {
	Name           *string `json:"name,omitempty"`
	Description    *string `json:"description,omitempty"`
	Repository     *string `json:"repository,omitempty"`
	RepositoryType *string `json:"repository_type,omitempty"`
	Website        *string `json:"website,omitempty"`
}

// SoftwarePatchFromJSON build a user from a JSON body
func SoftwarePatchFromJSON(data io.Reader) (*SoftwarePatch, *AppError) {
	var o SoftwarePatch
	err := FromJSON(&o, data)
	return &o, err
}

// NewSoftware return a new user based on patch data
func (o *SoftwarePatch) NewSoftware(userID uint64) *Software {
	return &Software{
		Name:           *o.Name,
		Description:    *o.Description,
		Repository:     *o.Repository,
		RepositoryType: *o.RepositoryType,
		Website:        *o.Website,
		CreatedBy:      userID,
	}
}
