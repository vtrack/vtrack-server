package model

import (
	"io"
	"net/http"
	"strings"
)

// Category structure
type Category struct {
	Name string `json:"name" binding:"required"`
}

// ToJSON build the JSON body from a struct
func (o *Category) ToJSON() []byte {
	return ToJSON(o)
}

// PreSave clean object before saving to store
func (o *Category) PreSave() *AppError {
	o.Name = strings.ToLower(o.Name)
	return nil
}

// IsValid return wether the category data is valid
func (o *Category) IsValid() *AppError {
	if o.Name == "" {
		return NewAppError("model.category", "IsValid", nil, "Name is empty", http.StatusBadRequest)
	}
	return nil
}

// CategoryFromJSON build a user from a JSON body
func CategoryFromJSON(data io.Reader) (*Category, *AppError) {
	var o Category
	err := FromJSON(&o, data)
	return &o, err
}

// Categories structure
type Categories []Category

// ToJSON build the JSON body from the struct
func (o *Categories) ToJSON() []byte {
	return ToJSON(o)
}
