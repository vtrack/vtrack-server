package model

import (
	"io"
	"net/http"
)

// Registration structure
type Registration struct {
	Username             string `json:"username" binding:"required"`
	Email                string `json:"email" binding:"required"`
	EmailConfirmation    string `json:"email_confirmation" binding:"required"`
	Password             string `json:"password" binding:"required"`
	PasswordConfirmation string `json:"password_confirmation" binding:"required"`
}

// RegistrationFromJSON build a registration from a JSON body
func RegistrationFromJSON(data io.Reader) (*Registration, *AppError) {
	var o Registration
	err := FromJSON(&o, data)
	return &o, err
}

// IsValid return wether the registration data is valid
func (o *Registration) IsValid() *AppError {

	if o.Username == "" {
		return NewAppError("model.registration", "IsValid", nil, "Username is empty", http.StatusBadRequest)
	}

	if o.Email == "" {
		return NewAppError("model.registration", "IsValid", nil, "Email is empty", http.StatusBadRequest)
	}

	if o.Password == "" {
		return NewAppError("model.registration", "IsValid", nil, "Password is empty", http.StatusBadRequest)
	}

	if o.Email != o.EmailConfirmation {
		return NewAppError("model.registration", "IsValid", nil, "Email doesn't match email_confirmation", http.StatusBadRequest)
	}

	if o.Password != o.PasswordConfirmation {
		return NewAppError("model.registration", "IsValid", nil, "Password doesn't match password_confirmation", http.StatusBadRequest)
	}
	return nil
}

// NewUser return a new user based on registration data
func (o *Registration) NewUser() *User {
	return &User{
		Username: o.Username,
		Email:    o.Email,
		Password: o.Password,
	}
}
