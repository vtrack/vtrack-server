package model

import "strings"

const (
	// RoleAdmin role key
	RoleAdmin = "admin"
)

// Roles type
type Roles string

// NewRoles build a new set of roles
func NewRoles(roles ...string) Roles {
	var result string
	for _, role := range roles {
		result = result + strings.ToLower(role) + ":"
	}
	return Roles(strings.TrimSuffix(result, ":"))
}

// ToList return a slice of Roles
func (o Roles) ToList() []string {
	return strings.Split(string(o), ":")
}

// HasRole check wether roles has the asked role
func (o Roles) HasRole(role string) bool {
	for _, v := range o.ToList() {
		if v == role {
			return true
		}
	}
	return false
}
