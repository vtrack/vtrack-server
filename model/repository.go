package model

// AllowedRepositoryTypes contains the list of allowed repository types
var AllowedRepositoryTypes = []string{RepositoryTypeGithub}

const (
	// RepositoryTypeGithub constant
	RepositoryTypeGithub = "github"
)
