package model

import (
	"strings"
	"testing"
)

func TestAuth(t *testing.T) {
	auth, err := AuthFromJSON(strings.NewReader(`{"username":"username","password":"password"}`))
	if err != nil {
		t.Error(err)
	}

	if err := auth.IsValid(); err != nil {
		t.Error(err)
	}

	auth, err = AuthFromJSON(strings.NewReader(`{"username":"","password":"password"}`))
	if err != nil {
		t.Error(err)
	}

	if err := auth.IsValid(); err != nil &&
		err.Message != "Username is empty" {
		t.Error(err)
	}

	auth, err = AuthFromJSON(strings.NewReader(`{"username":"username","password":""}`))
	if err != nil {
		t.Error(err)
	}

	if err := auth.IsValid(); err != nil &&
		err.Message != "Password is empty" {
		t.Error(err)
	}
}
