package model

import (
	"errors"
	"net/http"
	"testing"
)

func TestAppError(t *testing.T) {
	err := NewAppError("test.path", "Test", nil, "message", http.StatusUnauthorized)
	if err.String() != "message (test.path.Test) [401]" {
		t.Error(err.String())
	}
	if string(err.ToJSON()) != `{"message":"message","status_code":401}` {
		t.Error(err.String())
	}

	err = NewInternalAppError("test.path", "Test", errors.New("error"))
	if err.String() != "error (test.path.Test)" {
		t.Error(err.String())
	}
	if string(err.ToJSON()) != `{"message":"Internal server error","status_code":500}` {
		t.Error(err.String())
	}
}
