package model

import (
	"strings"
	"testing"
)

func TestFromJSON(t *testing.T) {
	for _, tt := range []struct {
		Body string
		Fail bool
	}{
		{`{"name":"Test"}`, false},
		{`{"name":"Test}`, true},
	} {
		o := &Category{}
		if err := FromJSON(o, strings.NewReader(tt.Body)); err != nil {
			if !tt.Fail {
				t.Error(err)
			}
		}
	}
}
