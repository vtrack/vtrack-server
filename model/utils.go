package model

import (
	"net/http"
	"net/mail"
)

const (
	// MinUsernameLength constant
	MinUsernameLength = 4
	// MaxUsernameLength constant
	MaxUsernameLength = 100
	// MinPasswordLength constant
	MinPasswordLength = 8
	// MaxPasswordLength constant
	MaxPasswordLength = 100
)

// IsValidEmail check wether the email is valid
func IsValidEmail(email string) *AppError {
	if email == "" {
		return NewAppError("model.user", "IsValid", nil, "Email is empty", http.StatusBadRequest)
	}

	parseEmail := func(email string) bool {
		if addr, err := mail.ParseAddress(email); err != nil {
			return false
		} else if addr.Name != "" {
			// mail.ParseAddress accepts input of the form "Billy Bob <billy@example.com>" which we don't allow
			return false
		}
		return true
	}

	if !parseEmail(email) {
		return NewAppError("model.user", "IsValid", nil, "Email is not valid", http.StatusBadRequest)
	}
	return nil
}

// IsValidUsername check wether the username is valid
func IsValidUsername(username string) *AppError {
	if username == "" {
		return NewAppError("model.utils", "IsValidUsername", nil, "Username is empty", http.StatusBadRequest)
	}
	if len(username) < MinUsernameLength ||
		len(username) > MaxUsernameLength {
		return NewAppError("model.utils", "IsValidUsername", nil, "Username length is not valid", http.StatusBadRequest)
	}
	if !validUsername.MatchString(username) {
		return NewAppError("model.utils", "IsValidUsername", nil, "Username is not valid", http.StatusBadRequest)
	}
	return nil
}

// IsValidPassword check wether the password is valid
func IsValidPassword(password string) *AppError {
	if password == "" {
		return NewAppError("model.user", "IsValid", nil, "Password is empty", http.StatusBadRequest)
	}
	if len(password) < MinPasswordLength ||
		len(password) > MaxPasswordLength {
		return NewAppError("model.user", "IsValid", nil, "Password length is not valid", http.StatusBadRequest)
	}
	return nil
}
