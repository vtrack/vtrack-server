package model

import (
	"io"
	"net/http"
)

// Subscription structure
type Subscription struct {
	Model

	UserID     uint64 `json:"-" db:"appuser_id"`
	SoftwareID uint64 `json:"-" db:"software_id"`

	Severity string `json:"severity" db:"severity"`

	*Software `json:",inline" db:"-"`
	Tags      *Tags `json:"tags,omitempty" db:"-"`
}

// ToJSON build the JSON body from a struct
func (o *Subscription) ToJSON() []byte {
	return ToJSON(o)
}

// IsValid return wether the software data is valid
func (o *Subscription) IsValid() *AppError {
	if o.Severity != "major" &&
		o.Severity != "minor" &&
		o.Severity != "patch" {
		return NewAppError("model.subscription", "IsValid", nil, "unknown severity level", http.StatusBadRequest)
	}
	return nil
}

// FromPatch merge patch subscription into current subscription
func (o *Subscription) FromPatch(patch *SubscriptionPatch) {
	if patch.Severity != nil {
		o.Severity = *patch.Severity
	}
}

// SubscriptionFromJSON build a user from a JSON body
func SubscriptionFromJSON(data io.Reader) (*Subscription, *AppError) {
	var o Subscription
	err := FromJSON(&o, data)
	return &o, err
}

// Subscriptions structure
type Subscriptions []Subscription

// ToJSON build the JSON body from the struct
func (o *Subscriptions) ToJSON() []byte {
	return ToJSON(o)
}

// SubscriptionPatch structure
type SubscriptionPatch struct {
	Severity *string `json:"severity"`
}

// SubscriptionPatchFromJSON build a user from a JSON body
func SubscriptionPatchFromJSON(data io.Reader) (*SubscriptionPatch, *AppError) {
	var o SubscriptionPatch
	err := FromJSON(&o, data)
	return &o, err
}

// NewSubscription return a new subscription based on patch data
func (o *SubscriptionPatch) NewSubscription(userID uint64, softwareID uint64) *Subscription {
	return &Subscription{
		UserID:     userID,
		SoftwareID: softwareID,
		Severity:   *o.Severity,
	}
}
