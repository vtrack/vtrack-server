package model

import (
	"net/http"

	"gitlab.com/vtrack/vtrack-server/utils"
)

// AppError structure
type AppError struct {
	Path          string `json:"-"`           // File path where the error occured
	In            string `json:"-"`           // Function name where the error occured
	InternalError error  `json:"-"`           // Admin error that will be logged on server side
	Message       string `json:"message"`     // End user message that describe the error
	StatusCode    int    `json:"status_code"` // HTTP status code that is supposed to be returned if error occurs
}

// NewAppError build a new app error
func NewAppError(path string, in string, err error, message string, statusCode int) *AppError {
	return &AppError{
		Path:          path,
		In:            in,
		InternalError: err,
		Message:       message,
		StatusCode:    statusCode,
	}
}

// String return a formatted string
func (o *AppError) String() string {
	if o.InternalError == nil {
		return o.Message + " (" + o.Path + "." + o.In + ") [" + utils.IntToString(o.StatusCode) + "]"
	}
	return o.InternalError.Error() + " (" + o.Path + "." + o.In + ")"
}

// ToJSON build the JSON body from a struct
func (o *AppError) ToJSON() []byte {
	return ToJSON(o)
}

// NewInternalAppError build a new internal app error
func NewInternalAppError(path string, in string, err error) *AppError {
	return NewAppError(path, in, err, "Internal server error", http.StatusInternalServerError)
}
