package model

import (
	"regexp"
	"strings"

	"gitlab.com/vtrack/vtrack-server/utils"
)

// Version is a struct defining a version x.y.z
type Version struct {
	Major      string
	Minor      string
	Patch      string
	PreRelease string
}

var rgxVersion = regexp.MustCompile("(\\d+\\.?)(\\d+\\.)?(\\d)?")

// NewVersion parse a string into a new version
func NewVersion(s string) *Version {
	v := &Version{}

	if s == "" {
		return v
	}

	matches := rgxVersion.FindAllString(s, 1)

	if len(matches) < 1 {
		return v
	}

	r := strings.Split(matches[0], ".")
	if len(r) > 0 {
		v.Major = r[0]
	}
	if len(r) > 1 {
		v.Minor = r[1]
	}
	if len(r) > 2 {
		v.Patch = r[2]
	}
	return v
}

// Compare new version to current and extract upgrade severity
func (v *Version) Compare(n *Version) string {
	if utils.StringToInt(n.Major) > utils.StringToInt(v.Major) {
		return "major"
	}
	if utils.StringToInt(n.Minor) > utils.StringToInt(v.Minor) {
		return "minor"
	}
	if utils.StringToInt(n.Patch) > utils.StringToInt(v.Patch) {
		return "patch"
	}
	return ""
}

// String allows to print a version
func (v *Version) String() string {
	var result string
	if v.Major != "" {
		result += v.Major
	}
	if v.Minor != "" {
		result += "." + v.Minor
	}
	if v.Patch != "" {
		result += "." + v.Patch
	}
	return result
}
